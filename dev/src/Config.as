package
{

	import com.dotr.core.controller.RequestSaveDataCommand;
	import com.dotr.core.controller.RequestSaveImageCommand;
	import com.dotr.core.controller.RequestSaveLocalDataCommand;
	import com.dotr.core.model.CoreModel;
	import com.dotr.core.remote.service.CoreService;
	import com.dotr.core.signals.ReceiveLoadLocalDataSignal;
	import com.dotr.core.signals.ReceiveSaveDataSignal;
	import com.dotr.core.signals.ReceiveSaveImageSignal;
	import com.dotr.core.signals.ReceiveSaveLocalDataSignal;
	import com.dotr.core.signals.RequestSaveDataSignal;
	import com.dotr.core.signals.RequestSaveImageSignal;
	import com.dotr.core.signals.RequestSaveLocalDataSignal;
	import com.dotr.module.home.view.HomeModule;
	import com.dotr.module.home.view.MenuView;
	import com.dotr.module.welcome.view.WelcomeModule;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.AppConfig;
	import com.sleepydesign.robotlegs.modules.ModuleTransition;
	import com.sleepydesign.robotlegs.shells.ShellContext;

	import flash.display.MovieClip;
	import flash.external.ExtensionContext;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.utils.getQualifiedClassName;

	import net.pirsquare.m.DeviceUtil;

	import org.robotlegs.base.SignalCommandMap;
	import org.robotlegs.core.IInjector;
	import org.robotlegs.core.IViewMap;

	public class Config
	{

		// App --------------------------------------------------------------
		public static const VERSION:String = "1.0.0"; //DataProxy.getData("VERSION");
		public static const SERVER_DATA:String = "http://192.168.0.1:8090/dotr/web/";
		//public static const SERVER_DATA:String = "http://aisystem.dyndns.biz:8090/dotr/web/";

		// Setting -----------------------------------------------------------
		public static const GUI_SIZE:Rectangle = new Rectangle(0, 0, 800, 1280);

		//for iOS
		//public static var work_space:Rectangle = new Rectangle(0, 0,768,1024);
		//for note 10.1
		public static var work_space:Rectangle = new Rectangle(0, 0, 800, 1232);

		public static const ratio:Number = Capabilities.screenResolutionY / GUI_SIZE.height;

		public static var isDebug:Boolean = false;
		public static var isDownloadImage:Boolean = false;
		public static var isDownloadImageFromLocal:Boolean = true;
		public static var isDynamic:Boolean = false;

		// Styles --------------------------------------------------------------
		private static var _shellContext:ShellContext;

		public static var panel:MovieClip;

		public static function get shellContext():ShellContext
		{
			return _shellContext;
		}

		public static function get M10_WELCOME_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[0]);
		}

		public static function get M20_HOME_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[1]);
		}


		// init --------------------------------------------------------------

		public static function init():void
		{
			// predefine layers
			AppConfig.layers = Vector.<String>([AppConfig.SYSTEM_LAYER, AppConfig.BG_LAYER, AppConfig.APP_LAYER, AppConfig.TOP_LAYER, AppConfig.LIGHTBOX_LAYER]);

			// predefine modules
			AppConfig.modules = Vector.<Class>([WelcomeModule, HomeModule, MenuView]);

			// custom transitions
			AppConfig.transition = new ModuleTransition;

			// do before startup thingy here
			AppConfig.beforeStartup = beforeStartup;

			// do startup thingy here
			AppConfig.startup = startup;

			// do startup thingy here
			AppConfig.onStartup = onStartup;

			// set default modul
			AppConfig.DEFAULT_MODULE_NAME = M10_WELCOME_MODULE;
		}

		public static function macID():String
		{
			var date:Date = new Date();
			var dateString:String = date.fullYear.toString() + "-" + ((date.month <= 9) ? ('0' + date.month.toString()) : date.month.toString()) + '-' + ((date.date <= 9) ? ('0' + date.date.toString()) : date.date.toString()) + ' ' + ((date.hours <= 9) ? ('0' + date.hours.toString()) : date.hours.toString()) + ':' + ((date.minutes <= 9) ? ('0' + date.minutes.toString()) : date.minutes.toString()) + ':' + ((date.seconds <= 9) ? ('0' + date.seconds.toString()) : date.seconds.toString());

			var str:String = Capabilities.os + " : " + dateString + " : ";

			return str;
		}

		private static function startup(viewMap:IViewMap, injector:IInjector, signalCommandMap:SignalCommandMap):void
		{
			injector.mapSingleton(LightBoxSignal);
			injector.mapSingleton(CoreModel);
			injector.mapSingleton(CoreService);
			injector.mapSingleton(ReceiveSaveLocalDataSignal);
			injector.mapSingleton(ReceiveLoadLocalDataSignal);
			injector.mapSingleton(ReceiveSaveDataSignal);
			injector.mapSingleton(ReceiveSaveImageSignal);

			viewMap.mapType(LightBoxModule);

			signalCommandMap.mapSignalClass(RequestSaveDataSignal, RequestSaveDataCommand, false, ['raw', 'file_name']);
			signalCommandMap.mapSignalClass(RequestSaveLocalDataSignal, RequestSaveLocalDataCommand, false, ['raw', 'file_name']);
			signalCommandMap.mapSignalClass(RequestSaveImageSignal, RequestSaveImageCommand, false, ['ba']);
		}

		private static function beforeStartup(shellContext:ShellContext):void
		{
			shellContext.layers[AppConfig.LIGHTBOX_LAYER].addChild(new LightBoxModule);
		}

		private static function onStartup(shellContext:ShellContext):void
		{
			_shellContext = shellContext;
		}
	}
}


