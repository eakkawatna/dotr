package com.dotr.module.welcome.signals
{
	import org.osflash.signals.Signal;

	public class RequestInitAppDataSignal extends Signal
	{
		public function RequestInitAppDataSignal()
		{

			super();
		}
	}
}
