package com.dotr.module.welcome.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	import Dialog.error_sp;

	import org.osflash.signals.Signal;

	public class ErrorDialog extends SDSprite
	{
		public var tryAgainSignal:Signal = new Signal();
		private var _skin:Sprite;

		public function ErrorDialog(errorTXT:String)
		{
			_skin = new Dialog.error_sp;
			addChild(_skin);

			desc_tf.text = errorTXT;
			addEventListener(MouseEvent.MOUSE_DOWN, onDown);
		}

		public function get desc_tf():TextField
		{
			return _skin.getChildByName("desc_tf") as TextField;
		}

		protected function onDown(event:MouseEvent):void
		{
			if (event.target.name == "try_again_btn")
			{
				tryAgainSignal.dispatch();
				dispose();
			}
		}

		public function dispose():void
		{
			tryAgainSignal.removeAll();
			removeEventListener(MouseEvent.MOUSE_DOWN, onDown);
			super.destroy();
		}
	}
}
