package com.dotr.module.welcome.view
{

	import com.dotr.module.welcome.WelcomeContext;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import Dialog.preload_sp;
	
	import org.robotlegs.core.IInjector;

	public class WelcomeModule extends ModuleBase
	{
		private var _skin:Sprite;

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new WelcomeContext(this, value);
		}

		// -----------------------------------------------------------------------------------------------------------------------

		override public function create():void
		{
			var rect:Rectangle;
			if(Config.isDynamic){
				rect =  Config.work_space;
			}else{
				rect = Config.GUI_SIZE;
			}
			
			graphics.beginFill(0xFFFFFF,1);
			graphics.drawRect(0,0,rect.width,rect.height);
			graphics.endFill();
			
			_skin = new Dialog.preload_sp;
			
			if(Config.isDynamic){
			_skin.scaleX = _skin.scaleY = Config.ratio;
			_skin.x =rect.width/2- _skin.width/2;
			}
			
			addChild(_skin);
		}

		public function get desc_tf():TextField
		{
			return _skin.getChildByName("desc_tf") as TextField;
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{

			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			dispose();

			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

	}
}


