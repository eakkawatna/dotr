package com.dotr.module.welcome.view
{
	import com.dotr.core.component.DialogComponent;
	import com.dotr.core.model.CoreModel;
	import com.dotr.core.signals.ReceiveLoadLocalDataSignal;
	import com.dotr.module.welcome.model.WelcomeModel;
	import com.dotr.module.welcome.signals.ReceiveInitAppDataSignal;
	import com.dotr.module.welcome.view.component.ErrorDialog;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.display.MovieClip;
	import flash.events.StatusEvent;
	import flash.net.URLRequest;

	import air.net.URLMonitor;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class WelcomeMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:WelcomeModule;

		[Inject]
		public var model:WelcomeModel;

		[Inject]
		public var receiveInitAppDataSignal:ReceiveInitAppDataSignal;

		[Inject]
		public var receiveLoadLocalDataSignal:ReceiveLoadLocalDataSignal;

		// create ------------------------------------------------------------------

		private var _preload:MovieClip;
		private var _monitor:URLMonitor;
		private var _dialogComponent:DialogComponent;

		private var _error:ErrorDialog;


		override public function onRegister():void
		{
			trace("! " + this + ".start");
			module.create();
			checkInternet();

			model.updateSignal.add(updateProgress);
			model.updateSignal.dispatch(0);


			model.errorSignal.add(onError);
		}

		private function onError():void
		{
			if (_error)
			{
				_error.dispose();
			}
			_error = new ErrorDialog("ระบบมีปัญหา\nกรุณาลองใหม่อีกครั้ง");
			_error.tryAgainSignal.addOnce(loadData);
			module.addChild(_error);
		}

		private function checkInternet():void
		{
			_monitor = new URLMonitor(new URLRequest('http://www.adobe.com'));
			_monitor.addEventListener(StatusEvent.STATUS, netConnectivity);
			_monitor.start();
		}

		private function updateProgress(value:int):void
		{
			module.desc_tf.text = value + "%";
		}

		protected function netConnectivity(e:StatusEvent):void
		{
			if (_monitor.available)
				coreModel.isOnline = true;
			else
				coreModel.isOnline = false;

			if (Config.isDebug)
				coreModel.isOnline = false;

			_monitor.stop();
			_monitor.removeEventListener(StatusEvent.STATUS, netConnectivity);
			loadData();
		}

		private function loadData():void
		{
			receiveInitAppDataSignal.removeAll();
			receiveInitAppDataSignal.addOnce(onComplete);
			model.getData();
		}

		private function onComplete():void
		{
			appModel.viewManager.viewID = Config.M20_HOME_MODULE;
		}


		// event ------------------------------------------------------------------

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
