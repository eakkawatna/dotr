package com.dotr.module.welcome.service
{
	import com.dotr.core.model.CoreModel;
	import com.dotr.core.model.data.ActivityData;
	import com.dotr.core.model.data.AmphurData;
	import com.dotr.core.model.data.ClusterData;
	import com.dotr.core.model.data.FacilitiesData;
	import com.dotr.core.model.data.ProvinceData;
	import com.dotr.core.model.data.RegionData;
	import com.dotr.core.model.data.SubDistrictData;
	import com.dotr.core.model.data.SubTouristTypeData;
	import com.dotr.core.model.data.TouristData;
	import com.dotr.core.model.data.TouristTypeData;
	import com.dotr.core.model.data.UtilityData;
	import com.dotr.module.welcome.model.WelcomeModel;
	import com.dotr.module.welcome.signals.ReceiveInitAppDataSignal;
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.net.LoaderUtil;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;

	import __AS3__.vec.Vector;

	import by.blooddy.crypto.image.JPEGEncoder;

	import net.pirsquare.m.MobileFileUtil;

	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;

	public class WelcomeService extends Actor
	{

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var model:WelcomeModel;

		[Inject]
		public var receiveInitAppDataSignal:ReceiveInitAppDataSignal;

		private var _loop:int;
		private var _count:int;
		private var _touristDatas:Vector.<TouristData>;
		private var _image_count:uint = 0;
		private var _loader:Loader;

		public function init():void
		{
			loadConfig();
		}

		private function loadConfig():void
		{
			var urlVar:URLVariables = new URLVariables()
			urlVar.offset = 0;

			if (coreModel.isOnline)
				loadJson(Config.SERVER_DATA + "dataJson4.php", urlVar, URLRequestMethod.POST).addOnce(onCompleteConfig);
			else
				MobileFileUtil.open("dotr/dataJson4.txt").addOnce(function(ba:ByteArray):void
				{
					onCompleteConfig(ba.toString());
				});

			model.updateSignal.dispatch(5);
		}

		private function onCompleteConfig(raw:String):void
		{

			var datas:Array = decode(raw);
			// chack error
			if (!datas)
			{
				model.errorSignal.dispatch();
				return;
			}

			var value:int = datas[0].tour_total;

			_loop = Math.ceil(value / 250);

			//save data
			if (coreModel.isOnline)
				saveFile(raw, "dataJson4");

			//
			coreModel.tour_total = datas[0].tour_total;
			coreModel.base_tour_total = datas[0].tour_total;

			loadAppData();
		}

		private function loadAppData():void
		{
			var urlVar:URLVariables = new URLVariables()
			urlVar.offset = 250;
			urlVar.limit = 250 * _count;

			if (coreModel.isOnline)
			{
				var value:int = (_count / _loop) * 80;
				model.updateSignal.dispatch(value);

				loadJson(Config.SERVER_DATA + "dataJson1.php", urlVar, URLRequestMethod.POST).addOnce(onLoadTouristDataComplete);
			}
			else
				MobileFileUtil.open("dotr/dataJson1.txt").addOnce(function(ba:ByteArray):void
				{
					model.updateSignal.dispatch(80);
					onLoadTouristDataComplete(ba.toString());
				});
		}

		private function onLoadTouristDataComplete(raw:String):void
		{

			var datas:Array = decode(raw);
			// chack error
			if (!datas)
			{
				model.errorSignal.dispatch();
				return;
			}

			if (!_touristDatas)
				_touristDatas = new Vector.<TouristData>;

			for each (var obj:Object in datas)
			{
				var touristData:TouristData = new TouristData(obj.touristid, obj.tourist_typeid, obj.tourist_subtypeid, obj.touristcode, obj.tourist_pic, obj.touristname, obj.clusterid, obj.region_id, obj.provinceid, obj.amphorid, obj.districtid, obj.postcode, obj.latitude, obj.longtitude, obj.agencyid, obj.address, obj.website, obj.email, obj.space, obj.feature, obj.keyword, obj.telnumber, obj.date_open, obj.description, obj.counter_thai, obj.counter_visitor, obj.status, obj.tourist_typename, obj.province_name, obj.amphur_name, obj.district_name, obj.agencyname, parseActivityData(obj.activity), parseFacilitiesData(obj.facilities), parseUtilityData(obj.utility), obj.imagepath);
				_touristDatas.push(touristData);
			}

			_count++;

			coreModel.touristDatas = _touristDatas;

			if (!coreModel.isOnline || _count >= _loop)
			{
				var new_raw:String = encode(coreModel.touristDatas);
				if (coreModel.isOnline)
					saveFile(new_raw, "dataJson1");
				loadJSON3();
			}
			else
				loadAppData();
		}

		private function parseActivityData(datas:Array):Vector.<ActivityData>
		{
			var activityDatas:Vector.<ActivityData> = new Vector.<ActivityData>();
			for each (var obj:Object in datas)
			{
				activityDatas.push(new ActivityData(obj.activityid, obj.activityname, obj.activity_type_id, obj.activity_type_name));
			}

			return activityDatas;
		}

		private function parseFacilitiesData(datas:Array):Vector.<FacilitiesData>
		{
			var facilitiesDatas:Vector.<FacilitiesData> = new Vector.<FacilitiesData>();
			for each (var obj:Object in datas)
			{
				facilitiesDatas.push(new FacilitiesData(obj.facilitiesid, obj.facilitiesname, obj.facilitiestypeid, obj.facilities_type_name));
			}

			return facilitiesDatas;
		}

		private function parseUtilityData(datas:Array):Vector.<UtilityData>
		{
			var utilityDatas:Vector.<UtilityData> = new Vector.<UtilityData>();
			for each (var obj:Object in datas)
			{
				utilityDatas.push(new UtilityData(obj.utilityid, obj.utilityname, obj.utility_type_id, obj.utility_type_name));
			}

			return utilityDatas;
		}


		private function loadAllimage():void
		{
			var loader:Loader;
			var td:TouristData = coreModel.touristDatas[_image_count];

			var new_name:String = td.imagepath.replace("tour", "tour_thumb");
			var file:File = getImageFile(new_name);

			var value:int = 50 + _image_count / coreModel.touristDatas.length * 50;
			model.updateSignal.dispatch(value);

			if (file.exists)
			{
				file.cancel();
				ImageManager.loadImage("all", file.url, onComplete);
			}
			else
			{
				file.cancel();
				loader = new Loader();

				loader = LoaderUtil.loadBinaryAsBitmap(Config.SERVER_DATA + td.imagepath, function(event:Event):void
				{
					if (event.type == Event.COMPLETE)
					{
						saveImageFile(loader.contentLoaderInfo.content as Bitmap, td.imagepath)
					}
				});

				/*_var new_name:String = td.imagepath.replace("tour", "src_tour");
				var f:File = File.documentsDirectory.resolvePath("dotr/" + new_name);
				ImageManager.loadImage("all", f.url, function(src:Bitmap):void
				{
					saveImageFile(src, td.imagepath)
				});*/

			}

		}

		private function loadJSON3():void
		{
			model.updateSignal.dispatch(99);
			if (coreModel.isOnline)
				loadJson(Config.SERVER_DATA + "dataJson3.php", new URLVariables).addOnce(onLoadJSON3Complete);
			else
				MobileFileUtil.open("dotr/dataJson3.txt").addOnce(function(ba:ByteArray):void
				{
					onLoadJSON3Complete(ba.toString());
				});
		}

		private function onLoadJSON3Complete(raw:String):void
		{

			//save data
			var datas:Object = decode(raw);
			// chack error
			if (!datas)
			{
				model.errorSignal.dispatch();
				return;
			}

			coreModel.clusterDatas = parseClusterData(datas.cluster);
			coreModel.regionDatas = parseRegionData(datas.region);
			coreModel.provinceDatas = parseProvinceData(datas.province);
			coreModel.amphurDatas = parseAmphurData(datas.amphur);
			coreModel.subDistrictDatas = parseSubDistrictData(datas.subdistrict);
			coreModel.touristTypeDatas = parseTouristTypeData(datas.tourtype);
			coreModel.subTouristTypeDatas = parseSubTouristTypeDataData(datas.toursubtype);


			var new_raw:String = encode(datas);
			if (coreModel.isOnline)
				saveFile(new_raw, "dataJson3");


			// load image
			if (Config.isDownloadImage)
				loadAllimage();
			else
				receiveInitAppDataSignal.dispatch();

		}

		private function parseClusterData(datas:Array):Vector.<ClusterData>
		{
			var clusterDatas:Vector.<ClusterData> = new Vector.<ClusterData>;
			for each (var obj:Object in datas)
			{
				var clusterData:ClusterData = new ClusterData(obj.clusterid, obj.clustername, obj.cluster_total);
				clusterDatas.push(clusterData);
			}
			return clusterDatas;
		}

		private function parseRegionData(datas:Array):Vector.<RegionData>
		{
			var regionDatas:Vector.<RegionData> = new Vector.<RegionData>;
			for each (var obj:Object in datas)
			{
				var regionData:RegionData = new RegionData(obj.region_id, obj.region_name, obj.region_total);
				regionDatas.push(regionData);
			}
			return regionDatas;
		}

		private function parseProvinceData(datas:Array):Vector.<ProvinceData>
		{
			var provinceDatas:Vector.<ProvinceData> = new Vector.<ProvinceData>;
			for each (var obj:Object in datas)
			{
				var provinceData:ProvinceData = new ProvinceData(obj.province_id, obj.province_name, obj.region_id, obj.pv_total);
				provinceDatas.push(provinceData);
			}
			return provinceDatas;
		}

		private function parseAmphurData(datas:Array):Vector.<AmphurData>
		{
			var amphurDatas:Vector.<AmphurData> = new Vector.<AmphurData>;
			for each (var obj:Object in datas)
			{
				var amphurData:AmphurData = new AmphurData(obj.amphur_id, obj.amphur_name, obj.province_id);
				amphurDatas.push(amphurData);
			}
			return amphurDatas;
		}

		private function parseSubDistrictData(datas:Array):Vector.<SubDistrictData>
		{
			var subDistrictDatas:Vector.<SubDistrictData> = new Vector.<SubDistrictData>;
			for each (var obj:Object in datas)
			{
				var subDistrictData:SubDistrictData = new SubDistrictData(obj.district_id, obj.district_name, obj.amphur_id, obj.rovince_id);
				subDistrictDatas.push(subDistrictData);
			}
			return subDistrictDatas;
		}

		private function parseTouristTypeData(datas:Array):Vector.<TouristTypeData>
		{
			var touristTypeDatas:Vector.<TouristTypeData> = new Vector.<TouristTypeData>;
			for each (var obj:Object in datas)
			{
				var touristTypeData:TouristTypeData = new TouristTypeData(obj.tourist_typeid, obj.tourist_typename);
				touristTypeDatas.push(touristTypeData);
			}
			return touristTypeDatas;
		}

		private function parseSubTouristTypeDataData(datas:Array):Vector.<SubTouristTypeData>
		{
			var subTouristTypeDatas:Vector.<SubTouristTypeData> = new Vector.<SubTouristTypeData>;
			for each (var obj:Object in datas)
			{
				var subTouristTypeData:SubTouristTypeData = new SubTouristTypeData(obj.sub_tourist_type_id, obj.sub_tourist_type_name, obj.tourist_typeid);
				subTouristTypeDatas.push(subTouristTypeData);
			}
			return subTouristTypeDatas;
		}

		public function saveFile(raw:String, file_name:String):void
		{
			var file:File = getFile(file_name);

			var fileStream:FileStream = new FileStream();
			fileStream.openAsync(file, FileMode.WRITE);
			fileStream.writeUTFBytes(raw);
			fileStream.addEventListener(Event.CLOSE, function(e:Event):void
			{
				trace("complete : " + file_name);
				fileStream.close();
			});

		}

		public function getFile(file_name:String):File
		{
			var f:File;
			/*if (Config.isDownloadImageFromLocal)
				f = File.applicationStorageDirectory.resolvePath(file_name + ".txt");
			else*/
			f = File.documentsDirectory.resolvePath("dotr/" + file_name + ".txt");
			return f;
		}


		public function getImageFile(file_name:String):File
		{
			var f:File;
			if (Config.isDownloadImageFromLocal)
				f = File.applicationDirectory.resolvePath(file_name);
			else
				f = File.documentsDirectory.resolvePath("dotr/" + file_name);
			return f;
		}

		/// -------------

		public function decode(value:String):*
		{
			try
			{
				return JSON.parse(value);
			}
			catch (e:Error)
			{
				model.errorSignal.dispatch();
				return null;
			}


		}

		public function encode(value:*):String
		{
			return JSON.stringify(value);
		}

		public function loadJson(group_name:String, urlVariables:URLVariables, type:String = URLRequestMethod.GET):Signal
		{
			var completeSignal:Signal = new Signal(String);

			var date:Date = new Date();
			var dateString:String = date.fullYear.toString() + "-" + ((date.month <= 9) ? ('0' + date.month.toString()) : date.month.toString()) + '-' + ((date.date <= 9) ? ('0' + date.date.toString()) : date.date.toString()) + ' ' + ((date.hours <= 9) ? ('0' + date.hours.toString()) : date.hours.toString()) + ':' + ((date.minutes <= 9) ? ('0' + date.minutes.toString()) : date.minutes.toString()) + ':' + ((date.seconds <= 9) ? ('0' + date.seconds.toString()) : date.seconds.toString());

			urlVariables.client_time = dateString;
			// set URI
			var uri:String = group_name;

			// load!
			LoaderUtil.request(uri, urlVariables, function(event:Event):void
			{
				if (event.type == IOErrorEvent.IO_ERROR)
				{
					model.errorSignal.dispatch();
				}

				if (event.type == Event.COMPLETE)
				{
					var raw:String = event.target.data as String;
					//-tell any one listener
					completeSignal.dispatch(raw);
					//-clear signal
					completeSignal.removeAll();
					completeSignal = null;
				}
			}, URLLoaderDataFormat.TEXT, type);

			return completeSignal;
		}


		public function saveImageFile(src:Bitmap, file_name:String):void
		{
			var bitmapData:BitmapData = new BitmapData(800, 600, false, 0x00FF00);
			var bitmap:Bitmap = new Bitmap(bitmapData);

			var ratioH:Number = bitmapData.height / src.height;
			var ratioV:Number = bitmapData.width / src.width;

			if (ratioV > ratioH)
				ratioH = ratioV;
			else
				ratioV = ratioH;

			var offsetX:Number = bitmapData.width / 2 - (src.width * ratioH / 2);
			var offsetY:Number = bitmapData.height / 2 - (src.height * ratioV / 2);

			bitmap.bitmapData.draw(src, new Matrix(ratioH, 0, 0, ratioV, offsetX, offsetY), null, null, null, true);

			var ba:ByteArray = JPEGEncoder.encode(bitmapData, 40);
			var file:File = getImageFile(file_name);
			var fileStream:FileStream = new FileStream();
			fileStream.openAsync(file, FileMode.WRITE);
			fileStream.writeBytes(ba);
			fileStream.close();

			saveImageThumb(bitmap, file_name);
		}

		public function saveImageThumb(src:Bitmap, file_name:String):void
		{
			var bitmapData:BitmapData = new BitmapData(217, 150, false, 0x00FF00);

			var ratioH:Number = bitmapData.height / src.height;
			var ratioV:Number = bitmapData.width / src.width;

			if (ratioV > ratioH)
				ratioH = ratioV;
			else
				ratioV = ratioH;

			var offsetX:Number = bitmapData.width / 2 - (src.width * ratioH / 2);
			var offsetY:Number = bitmapData.height / 2 - (src.height * ratioV / 2);

			bitmapData.draw(src, new Matrix(ratioH, 0, 0, ratioV, offsetX, offsetY), null, null, null, true);
			var ba:ByteArray = JPEGEncoder.encode(bitmapData, 40);

			var new_name:String = file_name.replace("tour", "tour_thumb");
			var file:File = getImageFile(new_name);
			var fileStream:FileStream = new FileStream();
			fileStream.openAsync(file, FileMode.WRITE);
			fileStream.writeBytes(ba);
			fileStream.close();
			onComplete();
		}

		private function onComplete(src:Bitmap = null):void
		{
			if (_image_count < coreModel.touristDatas.length - 1)
			{
				_image_count++;
				loadAllimage();
			}
			else
			{
				receiveInitAppDataSignal.dispatch();
			}
		}
	}
}
