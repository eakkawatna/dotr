package com.dotr.module.welcome.model
{
	import com.dotr.module.welcome.signals.RequestInitAppDataSignal;

	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;

	public class WelcomeModel extends Actor
	{
		// internal ------------------------------------------------------------------

		[Inject]
		public var requestInitAppDataSignal:RequestInitAppDataSignal;


		// create --------------------------------------------------------------------
		public var loginCompleteSignal:Signal = new Signal(String);
		public var errorSignal:Signal = new Signal();
		public var updateSignal:Signal = new Signal(int);

		public var user:String;
		public var pass:String;


		public function getData():void
		{
			requestInitAppDataSignal.dispatch();
		}


	}
}
