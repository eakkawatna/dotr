package com.dotr.module.welcome.controller
{
	import com.dotr.module.welcome.service.WelcomeService;

	import org.robotlegs.mvcs.SignalCommand;

	public class RequestInitAppDataCommand extends SignalCommand
	{
		[Inject]
		public var service:WelcomeService;


		//-get carees data from server.
		override public function execute():void
		{
			service.init();
			//-
			super.execute();
		}
	}
}
