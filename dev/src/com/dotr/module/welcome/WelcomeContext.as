package com.dotr.module.welcome
{

	import com.dotr.module.welcome.controller.RequestInitAppDataCommand;
	import com.dotr.module.welcome.model.WelcomeModel;
	import com.dotr.module.welcome.service.WelcomeService;
	import com.dotr.module.welcome.signals.ReceiveInitAppDataSignal;
	import com.dotr.module.welcome.signals.RequestInitAppDataSignal;
	import com.dotr.module.welcome.view.WelcomeMediator;
	import com.dotr.module.welcome.view.WelcomeModule;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class WelcomeContext extends ModuleContext
	{
		public function WelcomeContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			injector.mapSingleton(WelcomeModel);
			injector.mapSingleton(WelcomeService);
			injector.mapSingleton(ReceiveInitAppDataSignal);

			signalCommandMap.mapSignalClass(RequestInitAppDataSignal, RequestInitAppDataCommand);

			mediatorMap.mapView(WelcomeModule, WelcomeMediator);

			super.startup();
		}

		override public function dispose():void
		{

			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);

			super.dispose();
		}
	}
}
