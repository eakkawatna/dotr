package com.dotr.module.home.view
{
	import com.greensock.TweenLite;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.view.ViewBase;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	import assets.m00_menu_sp;
	
	import org.osflash.signals.Signal;


	public class MenuView extends ViewBase
	{
		public static const CLUSTER:String = "CLUSTER";
		public static const MAP:String = "MAP";
		public static const CONTACT:String = "CONTACT";
		public static const ADMIN:String = "ADMIN";

		public var selectMenuSignal:Signal = new Signal(String);

		private var _skin:Sprite;
		private var _isSlide:Boolean;

		public function get isSlide():Boolean
		{
			return _isSlide;
		}

		private var _complete:Function;
		private var _block:Boolean = false;

		private var _block_sp:Sprite;


		public function create():void
		{
			var rect:Rectangle;
			
			if(Config.isDynamic){
				rect =  Config.work_space;
			}else{
				rect = Config.GUI_SIZE;
			}
			
			_block_sp = new Sprite();
			addChild(_block_sp);
			_block_sp.graphics.beginFill(0x000000, .8);
			_block_sp.graphics.drawRect(0, 0, rect.width, rect.height);
			_block_sp.graphics.endFill();

			_skin = new assets.m00_menu_sp;
			_skin.x = 0 - _skin.width;
			addChild(_skin);
		
			
			if(Config.isDynamic)
				_skin.scaleX = _skin.scaleY = Config.ratio;
			
			block = false;
		}

		public function slide(complete:Function):void
		{
			_complete = complete;

			if (_isSlide)
				return;

			_isSlide = true;

			var value:Number = (_skin.x == 0) ? 0 - _skin.width : 0;
			TweenLite.killTweensOf(_skin);
			TweenLite.to(_skin, .6, {x: value, onComplete: onComplete()});
		}


		public function get block():Boolean
		{
			return _block;
		}

		public function set block(value:Boolean):void
		{
			_block = value;

			if (_block)
			{
				_block_sp.visible = true;
				_block_sp.mouseEnabled = true;
			}
			else
			{
				_block_sp.visible = false;
				_block_sp.mouseEnabled = false;
			}
		}

		private function onComplete():void
		{
			_isSlide = false;

			if (_skin.x == 0)
				block = false;
			else
				block = true;

			_complete();
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

	}
}
