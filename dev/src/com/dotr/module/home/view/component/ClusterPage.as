package com.dotr.module.home.view.component
{
	import com.dotr.core.model.data.ClusterData;
	import com.dotr.core.model.data.TouristData;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;

	import org.osflash.signals.Signal;

	public class ClusterPage extends SDSprite
	{
		public var ROW:int = 3;
		public var selectSignal:Signal = new Signal(String);
		private var _contentScroll:ScrollController;
		private var _datas:Vector.<ClusterData>;
		private var _tds:Vector.<TouristData>;

		public function ClusterPage(datas:Vector.<ClusterData>, tds:Vector.<TouristData>)
		{
			_datas = datas;
			_tds = tds;
			_contentScroll = new ScrollController();
			initHandler();
		}

		private function initHandler():void
		{
			//addEventListener(MouseEvent.MOUSE_UP, onUp);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}

		// event ---------------------------------------------------------

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}


		protected function onRemove(event:Event):void
		{
			dispose();
		}

		// basic ---------------------------------------------------------

		public function create():void
		{
			//create new content
			var sp:Sprite = new Sprite();
			addChild(sp);

			var c_container:Sprite = new Sprite();
			sp.addChild(c_container);

			var i:int = 0;
			var c:int = 0;
			var border:int = 25;
			var columns:int = Math.ceil(_datas.length / ROW);
			var count:int = 0;
			for (c = 0; c < columns; c++)
			{
				for (i = 0; i < ROW; i++)
				{
					if (count >= _datas.length)
						continue;

					var cd:ClusterData = _datas[count];
					var td:TouristData = VectorUtil.getItemBy(_tds, cd.clusterid, "clusterid");
					var thumb:ClusterThumb = new ClusterThumb(cd.clusterid, cd.clustername, cd.cluster_total.toString(), count + 1, td.imagepath, selectSignal.dispatch);
					thumb.x = (thumb.width + border) * i;
					thumb.y = (thumb.height + border) * c;
					c_container.addChild(thumb);
					count++;
				}
			}

			var containerViewport:Rectangle = new Rectangle(0, 0, (thumb.width + border) * 3, (thumb.height + border) * 3 - border);
			if (c_container.height > containerViewport.height)
			{
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.verticalScrollingEnabled = true;
				_contentScroll.addScrollControll(c_container, sp, containerViewport);
			}


		}

		public function dispose():void
		{
			//kill progress
			ImageManager.kill("ClusterPage");

			selectSignal.removeAll();
			//	removeEventListener(MouseEvent.MOUSE_UP, onUp);
			_contentScroll.removeScrollControll();
			destroy();
		}
	}
}
