package com.dotr.module.home.view.component
{
	import com.dotr.core.model.data.ProvinceData;
	import com.dotr.core.model.data.TouristData;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	import org.osflash.signals.Signal;

	public class ProvincePage extends SDSprite
	{
		public const ROW:int = 3;
		public var selectSignal:Signal = new Signal(String);
		private var _contentScroll:ScrollController;
		private var _provinceDatas:Vector.<ProvinceData>;
		private var _tds:Vector.<TouristData>;

		public function ProvincePage(provinceDatas:Vector.<ProvinceData>, tds:Vector.<TouristData>)
		{
			_tds = tds;
			_provinceDatas = provinceDatas;
			_contentScroll = new ScrollController();
			_contentScroll.displayVerticalScrollbar = true;
			_contentScroll.verticalScrollingEnabled = true;
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}

		private function initHandler():void
		{
			addEventListener(MouseEvent.MOUSE_UP, onUp);
		}

		// event ---------------------------------------------------------

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
			initHandler();

		}

		protected function onUp(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				var pt:ProvinceThumb = event.target.parent.parent as ProvinceThumb;
				selectSignal.dispatch(pt.id);
			}
		}

		protected function onRemove(event:Event):void
		{
			dispose();
		}

		// basic ---------------------------------------------------------


		public function create():void
		{
			if(!_provinceDatas)
				return;
			
			var sp:Sprite = new Sprite();
			addChild(sp);

			var c_container:Sprite = new Sprite();
			sp.addChild(c_container);

			var i:int = 0;
			var c:int = 0;
			var border:int = 25;
			var columns:int = Math.ceil(_provinceDatas.length / ROW);
			var count:int = 0;

			for (c = 0; c < columns; c++)
			{
				for (i = 0; i < ROW; i++)
				{
					if (count >= _provinceDatas.length)
						continue;

					var pd:ProvinceData = _provinceDatas[count];
					var td:TouristData = VectorUtil.getItemBy(_tds, pd.province_name, "province_name");
					var thumb:ProvinceThumb = new ProvinceThumb(pd.province_id, pd.province_name, td.imagepath, count + 1, pd.pv_total);
					thumb.x = (thumb.width + border) * i;
					thumb.y = (thumb.height + border) * c;
					c_container.addChild(thumb);
					count++;
				}
			}

			var containerViewport:Rectangle = new Rectangle(0, 0, (thumb.width + border) * 3, (thumb.height + border) * 3 - border);
			if (c_container.height > containerViewport.height)
			{
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.verticalScrollingEnabled = true;
				_contentScroll.addScrollControll(c_container, sp, containerViewport);
			}

		}

		public function dispose():void
		{
			//kill progress
			ImageManager.kill("ProvincePage");

			selectSignal.removeAll();
			removeEventListener(MouseEvent.MOUSE_UP, onUp);
			_contentScroll.removeScrollControll();
			destroy();
		}
	}
}
