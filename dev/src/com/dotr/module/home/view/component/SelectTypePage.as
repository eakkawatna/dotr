package com.dotr.module.home.view.component
{
	import com.greensock.TweenLite;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	import assets.m00_select_type_sp;

	import org.osflash.signals.Signal;

	public class SelectTypePage extends SDSprite
	{
		public var selectSignal:Signal = new Signal(String);
		private var _skin:Sprite;

		public function SelectTypePage()
		{
			initHandler();
		}

		private function initHandler():void
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			addEventListener(MouseEvent.MOUSE_UP, onUp);
		}

		// event ---------------------------------------------------------

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}

		protected function onUp(event:MouseEvent):void
		{
			switch (event.target.name)
			{
				case "btn_1":
					selectSignal.dispatch("3");
					break;
				case "btn_2":
					selectSignal.dispatch("5");
					break;
				case "btn_3":
					selectSignal.dispatch("4");
					break;
				case "btn_4":
					selectSignal.dispatch("1");
					break;
				case "btn_5":
					selectSignal.dispatch("2");
					break;
				case "btn_6":
					selectSignal.dispatch("all");
					break;
			}
			dispose();
		}


		// basic ---------------------------------------------------------

		public function create():void
		{
			var rect:Rectangle;

			if (Config.isDynamic)
			{
				rect = Config.work_space;
			}
			else
			{
				rect = Config.GUI_SIZE;
			}

			//create new content
			var container:Sprite = new Sprite();
			container.name = "close_btn";
			addChild(container);
			container.graphics.beginFill(0x000000, 0.9);
			container.graphics.drawRect(0, 0, rect.width, rect.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			_skin = new assets.m00_select_type_sp;

			if (Config.isDynamic)
				_skin.scaleX = _skin.scaleY = Config.ratio;

			_skin.x = rect.width / 2 - _skin.width / 2;
			_skin.y = rect.height / 2 - _skin.height / 2;
			addChild(_skin);
		}

		private function onComplete():void
		{
			TweenLite.killTweensOf(_skin);

		}

		public function dispose():void
		{
			removeEventListener(MouseEvent.MOUSE_UP, onUp);
			destroy();
		}
	}
}
