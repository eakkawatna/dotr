package com.dotr.module.home.view.component
{
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.skins.MacLoadingClip;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;

	public class PreloadComponent extends SDSprite
	{
		public function PreloadComponent()
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}

		private function create():void
		{
			var rect:Rectangle;

			if (Config.isDynamic)
			{
				rect = Config.work_space;
			}
			else
			{
				rect = Config.GUI_SIZE;
			}

			graphics.beginFill(0x000000, 0.8);
			graphics.drawRect(0, 0, rect.width, rect.height);
			graphics.endFill();

			var container:Sprite = new Sprite();
			addChild(container);

			var macPreload:Sprite = new MacLoadingClip();
			macPreload.scaleX = macPreload.scaleY = 1.5;
			macPreload.x = rect.width / 2 - macPreload.width / 2;
			macPreload.y = rect.height / 2 - macPreload.height / 2;
			addChild(macPreload);

			if (Config.isDynamic)
				container.scaleX = container.scaleY = Config.ratio;
		}

		//destroy ---------------------------------------------

		public function dispose():void
		{
			destroy();
		}
	}
}


