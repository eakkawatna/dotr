package com.dotr.module.home.view.component
{
	import com.dotr.core.model.data.TouristData;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	import assets.m00_type_2_btn;
	import assets.m00_type_btn;

	import org.osflash.signals.Signal;

	public class LandMarkPage extends SDSprite
	{
		public var ROW:int = 3;
		public var selectSignal:Signal = new Signal(String);
		private var _contentScroll:ScrollController;
		private var _datas:Vector.<TouristData>;
		private var _index:int = 0;

		private var _type_2_btn2:Sprite;
		private var _page:Boolean;

		public function LandMarkPage(datas:Vector.<TouristData>, index:int, page:Boolean)
		{
			_page = page;
			_datas = datas;
			_index = index;
			_contentScroll = new ScrollController();
			initHandler();
		}

		private function initHandler():void
		{
			addEventListener(MouseEvent.MOUSE_UP, onUp);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}

		// event ---------------------------------------------------------

		protected function onUp(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				var lt:LandMarkThumb = event.target.parent.parent as LandMarkThumb;
				selectSignal.dispatch(lt.id);
				return;
			}

			if (event.target.name == "type_btn")
				selectSignal.dispatch("type");

			if (event.target.name == "type_2_btn")
				selectSignal.dispatch("page");
		}

		protected function onRemove(event:Event):void
		{
			dispose();
		}

		// basic ---------------------------------------------------------

		public function create():void
		{
			//create new content
			var sp:Sprite = new Sprite();
			addChild(sp);

			var c_container:Sprite = new Sprite();
			sp.addChild(c_container);

			var type_btn:Sprite = new assets.m00_type_btn;
			type_btn.name = "type_btn";
			type_btn.x = 510;
			type_btn.y = 840;
			addChild(type_btn);


			_type_2_btn2 = new assets.m00_type_2_btn;
			_type_2_btn2.name = "type_2_btn";
			_type_2_btn2.x = 0;
			_type_2_btn2.y = 840;
			addChild(_type_2_btn2);

			if (!_datas || _datas.length == 0)
				return;

			var i:int = 0;
			var c:int = 0;
			var border:int = 25;
			var columns:int = Math.ceil(_datas.length / ROW);
			var count:int = 0;
			for (c = 0; c < columns; c++)
			{
				for (i = 0; i < ROW; i++)
				{
					if (count >= _datas.length)
						continue;
					var t:TouristData = _datas[count];
					var thumb:LandMarkThumb = new LandMarkThumb(_index, t.touristid, t.touristname, t.province_name, t.imagepath);
					thumb.x = (thumb.width + border) * i;
					thumb.y = (thumb.height + border) * c;
					c_container.addChild(thumb);
					count++;
					_index++;
				}
			}

			if (_page)
			{
				_type_2_btn2.alpha = .3;
				_type_2_btn2.mouseEnabled = false;
			}

			var containerViewport:Rectangle = new Rectangle(0, 0, (thumb.width + border) * 3, (thumb.height + border) * 3 - border);
			if (c_container.height > containerViewport.height)
			{
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.verticalScrollingEnabled = true;
				_contentScroll.addScrollControll(c_container, sp, containerViewport);
			}

		}

		public function dispose():void
		{
			//kill progress
			ImageManager.kill("LandMarkPage");

			selectSignal.removeAll();
			removeEventListener(MouseEvent.MOUSE_UP, onUp);
			removeEventListener(MouseEvent.MOUSE_UP, onUp);
			_contentScroll.removeScrollControll();
			destroy();
		}
	}
}
