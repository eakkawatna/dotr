package com.dotr.module.home.view.component
{
	import com.dotr.core.model.data.AmphurData;
	import com.dotr.core.model.data.ClusterData;
	import com.dotr.core.model.data.ProvinceData;
	import com.dotr.core.model.data.RegionData;
	import com.dotr.core.model.data.SubDistrictData;
	import com.dotr.core.model.data.SubTouristTypeData;
	import com.dotr.core.model.data.TouristTypeData;
	import com.idealizm.display.components.SDComboBox;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import ActionBar.button_back;

	import __AS3__.vec.Vector;

	import assets.m00_action_bar_sp;
	import assets.m00_advance_sp;

	import org.osflash.signals.Signal;

	public class AdvanceSearchPage extends SDSprite
	{
		public var searchSignal:Signal = new Signal(Object);
		public var HEIGHT:int = 84;

		private var _skin:Sprite;
		private var _container:Sprite;
		private var _regions:Vector.<RegionData>;
		private var _clusters:Vector.<ClusterData>;
		private var _provinces:Vector.<ProvinceData>;
		private var _amphurs:Vector.<AmphurData>;
		private var _subDistrictDatas:Vector.<SubDistrictData>;
		private var _amphur_combo:SDComboBox;
		private var _subDistrict_combo:SDComboBox;
		private var _province_combo:SDComboBox;
		private var _head:Sprite;
		private var _region_combo:SDComboBox;

		private var _cluster_combo:SDComboBox;
		private var _touristTypeDatas:Vector.<TouristTypeData>;
		private var _subTouristTypeDatas:Vector.<SubTouristTypeData>;
		private var _tourist_type_combo:SDComboBox;
		private var _sub_tourist_type_combo:SDComboBox;
		private var _back_btn:Sprite;

		private var _content:Sprite;

		public function AdvanceSearchPage(regions:Vector.<RegionData>, clusters:Vector.<ClusterData>, provinces:Vector.<ProvinceData>, amphurs:Vector.<AmphurData>, subDistrictDatas:Vector.<SubDistrictData>, touristTypeDatas:Vector.<TouristTypeData>, subTouristTypeDatas:Vector.<SubTouristTypeData>)
		{
			_regions = regions;
			_clusters = clusters;
			_provinces = provinces;
			_amphurs = amphurs;
			_subDistrictDatas = subDistrictDatas;
			_touristTypeDatas = touristTypeDatas;
			_subTouristTypeDatas = subTouristTypeDatas;
			initHandler();
		}

		private function initHandler():void
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			addEventListener(MouseEvent.MOUSE_UP, onDown);
		}

		// event ---------------------------------------------------------

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}

		protected function onDown(event:MouseEvent):void
		{
			if (event.target.name == "detial_close_btn")
				dispose();
			if (event.target.name == "search_advance_btn")
				search();
		}

		private function search():void
		{
			var obj:Object = new Object();
			obj.cluster_id = (_cluster_combo.selectedItem) ? _cluster_combo.selectedItem.data : "";
			obj.region_id = (_region_combo.selectedItem) ? _region_combo.selectedItem.data : "";
			obj.province_id = (_province_combo.selectedItem) ? _province_combo.selectedItem.data : "";
			obj.amphur_id = (_amphur_combo.selectedItem) ? _amphur_combo.selectedItem.data : "";
			obj.subDistrict_id = (_subDistrict_combo.selectedItem) ? _subDistrict_combo.selectedItem.data : "";
			obj.tourist_type_id = (_tourist_type_combo.selectedItem) ? _tourist_type_combo.selectedItem.data : "";
			obj.sub_tourist_type_id = (_sub_tourist_type_combo.selectedItem) ? _sub_tourist_type_combo.selectedItem.data : "";
			obj.keyword = keyword_txt.text;
			searchSignal.dispatch(obj);
			dispose();
		}

		private function get title_txt():TextField
		{
			return _head.getChildByName("title_txt") as TextField;
		}

		private function get keyword_txt():TextField
		{
			return _skin.getChildByName("keyword_txt") as TextField;
		}

		private function getGuide(value:int):Sprite
		{
			return _skin.getChildByName("guide_" + value) as Sprite;
		}

		public function create():void
		{
			var rect:Rectangle;

			if (Config.isDynamic)
			{
				rect = Config.work_space;
			}
			else
			{
				rect = Config.GUI_SIZE;
			}

			var bg:Sprite = new Sprite();
			addChild(bg);
			bg.graphics.beginFill(0xffffff, 1);
			bg.graphics.drawRect(0, 0, rect.width, rect.height);
			bg.graphics.endFill();

			// skin
			_head = new assets.m00_action_bar_sp;

			//back_btn
			_back_btn = new ActionBar.button_back;
			_back_btn.name = "detial_close_btn";

			_content = new Sprite;

			addChild(_content);


			if (Config.isDynamic)
			{
				HEIGHT *= Config.ratio;
				_head.scaleX = _head.scaleY = Config.ratio;
				_back_btn.scaleX = _back_btn.scaleY = Config.ratio;
				_content.scaleX = _content.scaleY = Config.ratio;
			}

			_skin = new assets.m00_advance_sp;
			_content.addChild(_skin);

			_container = new Sprite;
			_container.y = _skin.height;
			_content.addChild(_container);

			title_txt.text = "ADVANCE SEARCH";
			setSDComboBox();

			keyword_txt.text = "";

			var action_bar:Sprite = new Sprite;
			action_bar.graphics.beginFill(0x007FFF, 1);
			action_bar.graphics.drawRect(0, 0, rect.width, HEIGHT);
			action_bar.graphics.endFill();

			addChild(action_bar);
			addChild(_head);
			addChild(_back_btn);

			_back_btn.x = rect.width - _back_btn.width;
			_head.x = 0;
			_head.y = HEIGHT / 2 - _head.height / 2;


			if (Config.isDynamic)
				_content.x = bg.width / 2 - Config.GUI_SIZE.width * Config.ratio / 2;
		}

		private function setSDComboBox():void
		{

			//////////////////////////////////////////
			var guide:Sprite = getGuide(1);
			var array:Array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var cd:ClusterData in _clusters)
			{
				array.push({label: cd.clustername, data: cd.clusterid});
			}
			_cluster_combo = new SDComboBox(null, guide.x, guide.y, "ทั้งหมด", array);
			_content.addChild(_cluster_combo);

			guide.parent.removeChild(guide);
			//////////////////////////////////////////

			guide = getGuide(2);
			array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var rd:RegionData in _regions)
			{
				array.push({label: rd.region_name, data: rd.region_id});
			}
			_region_combo = new SDComboBox(null, guide.x, guide.y, "ทั้งหมด", array);
			_region_combo.changeSignal.add(onChangeRegion);
			_content.addChild(_region_combo);

			guide.parent.removeChild(guide);
			//////////////////////////////////////////

			guide = getGuide(3);
			array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var pd:ProvinceData in _provinces)
			{
				array.push({label: pd.province_name, data: pd.province_id});
			}
			_province_combo = new SDComboBox(null, guide.x, guide.y, "ทั้งหมด", array);
			_province_combo.changeSignal.add(onChangeProvince);
			_content.addChild(_province_combo);

			guide.parent.removeChild(guide);
			//////////////////////////////////////////

			guide = getGuide(4);
			_amphur_combo = new SDComboBox(null, guide.x, guide.y, "ทั้งหมด", []);
			_amphur_combo.changeSignal.add(onChangeAmphur);
			_content.addChild(_amphur_combo);

			guide.parent.removeChild(guide);
			//////////////////////////////////////////

			guide = getGuide(5);
			_subDistrict_combo = new SDComboBox(null, guide.x, guide.y, "ทั้งหมด", []);
			_content.addChild(_subDistrict_combo);

			guide.parent.removeChild(guide);

			//////////////////////////////////////////

			guide = getGuide(6);
			array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var ttd:TouristTypeData in _touristTypeDatas)
			{
				array.push({label: ttd.tourist_typename, data: ttd.tourist_typeid});
			}
			_tourist_type_combo = new SDComboBox(null, guide.x, guide.y, "ทั้งหมด", array);
			_content.addChild(_tourist_type_combo);

			guide.parent.removeChild(guide);

			//////////////////////////////////////////

			guide = getGuide(7);
			array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var sttd:SubTouristTypeData in _subTouristTypeDatas)
			{
				array.push({label: sttd.sub_tourist_type_name, data: sttd.sub_tourist_type_id});
			}
			_sub_tourist_type_combo = new SDComboBox(null, guide.x, guide.y, "ทั้งหมด", array);
			_content.addChild(_sub_tourist_type_combo);

			guide.parent.removeChild(guide);

			trace("end -> SDComboBox");
		}

		private function onChangeRegion():void
		{
			var point:Point;
			if (_province_combo)
			{
				point = new Point(_province_combo.x, _province_combo.y);
				_province_combo.dispose();
			}
			var region_id:String = _region_combo.selectedItem.data;
			var array:Array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var pd:ProvinceData in _provinces)
			{
				if (pd.region_id == region_id)
					array.push({label: pd.province_name, data: pd.province_id});
			}

			_province_combo = new SDComboBox(null, point.x, point.y, "ทั้งหมด", array);
			_province_combo.changeSignal.add(onChangeProvince);
			_content.addChild(_province_combo);
		}

		private function onChangeAmphur():void
		{
			var point:Point;
			if (_subDistrict_combo)
			{
				point = new Point(_subDistrict_combo.x, _subDistrict_combo.y);
				_subDistrict_combo.dispose();
			}
			var amphur_id:String = _amphur_combo.selectedItem.data;
			var array:Array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var sd:SubDistrictData in _subDistrictDatas)
			{
				if (sd.amphur_id == amphur_id)
					array.push({label: sd.district_name, data: sd.amphur_id});
			}

			_subDistrict_combo = new SDComboBox(null, point.x, point.y, "ทั้งหมด", array);
			_content.addChild(_subDistrict_combo);
		}

		private function onChangeProvince():void
		{
			var point:Point;
			if (_amphur_combo)
			{
				point = new Point(_amphur_combo.x, _amphur_combo.y);
				_amphur_combo.dispose();
			}

			var province_id:String = _province_combo.selectedItem.data;
			var array:Array = [];
			array.push({label: "ทั้งหมด", data: ""});
			for each (var ad:AmphurData in _amphurs)
			{
				if (ad.province_id == province_id)
					array.push({label: ad.amphur_name, data: ad.amphur_id});
			}

			_amphur_combo = new SDComboBox(null, point.x, point.y, "ทั้งหมด", array);
			_amphur_combo.changeSignal.add(onChangeAmphur);
			_content.addChild(_amphur_combo);

		}

		public function dispose():void
		{
			//
			_cluster_combo.dispose();
			_region_combo.dispose();
			_province_combo.dispose();
			_amphur_combo.dispose();
			_subDistrict_combo.dispose();
			//
			removeEventListener(MouseEvent.MOUSE_UP, onDown);
			destroy();
		}

	}
}
