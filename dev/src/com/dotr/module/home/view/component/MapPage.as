package com.dotr.module.home.view.component
{
	import com.dotr.core.model.data.RegionData;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	import assets.m00_map_sp;

	import org.osflash.signals.Signal;

	public class MapPage extends SDSprite
	{
		public var selectSignal:Signal = new Signal(String);
		private var _skin:Sprite;
		private var _regionDatas:Vector.<RegionData>;

		public function MapPage(regionDatas:Vector.<RegionData>)
		{
			_regionDatas = regionDatas;
			initHandler();
		}

		private function initHandler():void
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			addEventListener(MouseEvent.MOUSE_UP, onUp);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
		}


		// event ---------------------------------------------------------

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}

		protected function onRemove(event:Event):void
		{
			dispose();
		}

		protected function onUp(event:MouseEvent):void
		{
			if (String(event.target.name).indexOf("btn") != -1)
			{
				var id:String = String(event.target.name).split("_")[1];
				selectSignal.dispatch(id);
			}
		}

		private function create():void
		{
			_skin = new assets.m00_map_sp;
			addChild(_skin);

			for each (var regionData:RegionData in _regionDatas)
			{
				var tf:TextField = region_tf(regionData.region_id);
				tf.text = regionData.region_total.toString();
			}
		}

		private function region_tf(value:String):TextField
		{
			return _skin.getChildByName("total_" + value + "_tf") as TextField;
		}

		private function dispose():void
		{
			selectSignal.removeAll();
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			removeEventListener(MouseEvent.MOUSE_UP, onUp);
		}
	}
}
