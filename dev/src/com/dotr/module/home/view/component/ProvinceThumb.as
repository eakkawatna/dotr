package com.dotr.module.home.view.component
{
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import assets.m00_thumb_province_sp;

	public class ProvinceThumb extends SDSprite
	{
		private var _skin:Sprite;
		private var _id:String;
		private var _content:Sprite;
		private var _path:String = "images/tour/4397/1.jpg";
		private var _loader:Loader;

		public function ProvinceThumb(id:String, name:String, path:String, index:int, total_landmark:int = 0)
		{
			_path = path.replace("tour", "tour_thumb");
			_id = id;
			_skin = new assets.m00_thumb_province_sp;
			addChild(_skin);
			scrollRect = new Rectangle(0, 0, _skin.width, _skin.height);

			province_txt.text = name;
			total_landmark_txt.text = total_landmark.toString();
			index_txt.text = index.toString();

			_content = new Sprite;
			image_sp.mask = mask_sp;
			image_sp.addChild(_content);

			loadImage();
			initHandler();

			cacheAsBitmap = true;
		}


		private function initHandler():void
		{
			addEventListener(MouseEvent.MOUSE_OVER, onOver);
			addEventListener(MouseEvent.MOUSE_OUT, onOut);
		}

		protected function onOut(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				event.target.alpha = 1;
			}
		}

		protected function onOver(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				event.target.alpha = .5;
			}
		}

		public function get id():String
		{
			return _id;
		}

		public function get index_txt():TextField
		{
			return _skin.getChildByName("index_txt") as TextField;
		}

		public function get province_txt():TextField
		{
			return _skin.getChildByName("province_txt") as TextField;
		}

		public function get total_landmark_txt():TextField
		{
			return _skin.getChildByName("total_landmark_txt") as TextField;
		}

		public function get image_sp():Sprite
		{
			return _skin.getChildByName("image_sp") as Sprite;
		}

		public function get mask_sp():Sprite
		{
			return _skin.getChildByName("mask_sp") as Sprite;
		}

		public function loadImage():void
		{
			var f:File;
			if (Config.isDownloadImageFromLocal)
				f = File.applicationDirectory.resolvePath(_path);
			else
				f = File.documentsDirectory.resolvePath("dotr/" + _path);
			ImageManager.loadImage("ProvincePage", f.url, onComplete);
		}

		private function onComplete(src:Bitmap):void
		{
			_content.addChild(src);
		}
	}
}
