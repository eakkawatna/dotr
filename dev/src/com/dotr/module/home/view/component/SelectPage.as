package com.dotr.module.home.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import Dialog.select_page_sp;

	import org.osflash.signals.Signal;

	public class SelectPage extends SDSprite
	{
		public var selectSignal:Signal = new Signal(int, int);
		private var _skin:Sprite;
		private var _content:Sprite;
		private var _total:int;

		public function SelectPage(total:int)
		{
			_total = total;
			initHandler();
		}

		private function initHandler():void
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			addEventListener(MouseEvent.MOUSE_UP, onUp);
		}

		// event ---------------------------------------------------------

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}

		protected function onUp(event:MouseEvent):void
		{
			if (event.target is PageItem)
			{
				var pi:PageItem = event.target as PageItem;
				selectSignal.dispatch(pi.start, pi.end);
			}

			dispose();
		}


		// basic ---------------------------------------------------------

		public function create():void
		{
			//create new content
			var container:Sprite = new Sprite();
			container.name = "close_btn";
			addChild(container);

			container.graphics.beginFill(0x000000, 1);
			container.graphics.drawRect(0, 0, Config.work_space.width, Config.work_space.height);
			container.graphics.endFill();

			var content:Sprite = new Sprite;
			if (Config.isDynamic)
				content.scaleX = content.scaleY = Config.ratio;
			addChild(content);

			_skin = new Dialog.select_page_sp;
			content.addChild(_skin);

			_content = new Sprite;
			_content.y = 200;
			content.addChild(_content);

			var total_item:int = Math.ceil(_total / 99);
			var loop:int = Math.ceil(total_item / 4);
			var page_start:int = 1;
			var page_end:int = (_total < 99) ? _total : 99;
			var counter_id:int = 0;

			for (var i:int = 0; i < loop; i++)
			{
				var column:int = (i == loop - 1) ? total_item % 4 : 4;
				if (column == 0)
					column = 4;
				for (var j:int = 0; j < column; j++)
				{
					var pi:PageItem = new PageItem(counter_id.toString(), page_start, page_end);
					pi.x = (pi.width + 5) * j;
					pi.y = (pi.height + 5) * i;
					_content.addChild(pi);

					if (counter_id == loop - 1)
					{
						page_start += 99;
						page_end += _total % 99;
					}
					else
					{
						page_start += 99;
						page_end += 99;
					}
					counter_id++;
				}

			}

			content.x = Config.work_space.width / 2 - content.width / 2;
		}


		public function dispose():void
		{
			removeEventListener(MouseEvent.MOUSE_UP, onUp);
			destroy();
		}
	}
}
