package com.dotr.module.home.view.component
{
	import com.dotr.core.model.data.ActivityData;
	import com.dotr.core.model.data.FacilitiesData;
	import com.dotr.core.model.data.TouristData;
	import com.dotr.core.model.data.UtilityData;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.net.LoaderUtil;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.utils.Dictionary;

	import ActionBar.button_back;

	import assets.m00_action_bar_sp;
	import assets.m00_image_container_sp;

	import org.osflash.signals.Signal;

	public class DetailPage extends SDSprite
	{
		public var completeSignal:Signal = new Signal;
		public var HEIGHT:int = 84;

		private var _skin:Sprite;
		private var _td:TouristData;
		private var _container:Sprite;
		private var _image:Sprite;
		private var _contentScroll:ScrollController;
		private var _loader:Loader;
		private var _back_btn:Sprite;

		public function DetailPage(td:TouristData)
		{
			_td = td;
			initHandler();

			_contentScroll = new ScrollController();
		}

		private function initHandler():void
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			addEventListener(MouseEvent.MOUSE_UP, onDown);
		}

		// event ---------------------------------------------------------

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);
			create();
		}

		protected function onDown(event:MouseEvent):void
		{
			if (event.target.name == "detial_close_btn")
				dispose();
		}

		private function get title_txt():TextField
		{
			return _skin.getChildByName("title_txt") as TextField;
		}

		// basic ---------------------------------------------------------

		public function create():void
		{
			var rect:Rectangle;

			if (Config.isDynamic)
			{
				HEIGHT *= Config.ratio;
				rect = Config.work_space;
			}
			else
			{
				rect = Config.GUI_SIZE;
			}

			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginFill(0xffffff, 1);
			container.graphics.drawRect(0, 0, rect.width, rect.height);
			container.graphics.endFill();

			// skin
			_skin = new assets.m00_action_bar_sp;

			//back_btn
			_back_btn = new ActionBar.button_back;
			_back_btn.name = "detial_close_btn";


			_container = new Sprite;
			_container.y = _skin.height;

			if (Config.isDynamic)
			{
				HEIGHT *= Config.ratio;
				rect = Config.work_space;
				_skin.scaleX = _skin.scaleY = Config.ratio;
				_back_btn.scaleX = _back_btn.scaleY = Config.ratio;
				_container.scaleX = _container.scaleY = Config.ratio;
				_container.x = container.width / 2 - Config.GUI_SIZE.width * Config.ratio / 2;
			}
			_container.x = 10;

			addChild(_container);
			setDescription();

			var action_bar:Sprite = new Sprite;
			action_bar.graphics.beginFill(0x007FFF, 1);
			action_bar.graphics.drawRect(0, 0, rect.width, HEIGHT);
			action_bar.graphics.endFill();

			addChild(action_bar);
			addChild(_skin);
			addChild(_back_btn);

			_back_btn.x = rect.width - _back_btn.width;
			_skin.y = HEIGHT / 2 - _skin.height / 2;
		}

		private function setDescription():void
		{
			title_txt.text = _td.touristname;

			var content:Sprite = new Sprite;
			_container.addChild(content);

			_image = new assets.m00_image_container_sp;
			_image.x = Config.GUI_SIZE.width / 2 - _image.width / 2;
			_image.y = 20;
			content.addChild(_image);


			var item:HistoryItem = new HistoryItem("ประวัติความเป็นมา", _td.description);
			item.x = Config.GUI_SIZE.width / 2 - item.width / 2;
			item.y = _image.y + _image.height + 20;
			content.addChild(item);

			var item2:HistoryItem = new HistoryItem("รายละเอียดข้อมูลทั่วไป", "<font color='#000000'>ประเภทแหล่งท่องเที่ยว : </font>" + _td.tourist_typename + "\n<font color='#000000'>ชื่อแหล่งท่องเที่ยว : </font>" + _td.touristname + "\n<font color='#000000'>ที่อยู่ : </font>" + _td.address + "  ตำบล/แขวง" + _td.district_name + " อำเภอ/เขต" + _td.amphur_name + "\n<font color='#000000'>จังหวัด : </font>" + _td.province_name + "\n<font color='#000000'>รหัสไปรษณีย์ : </font>" + _td.postcode + "\n<font color='#000000'>แหล่งข้อมูลอ้างอิง : </font>" + _td.website + "\n<font color='#000000'>อีเมล : </font>" + _td.email + "\n<font color='#000000'>เบอร์โทร : </font>" + _td.telnumber + "\n<font color='#000000'>หน่วยงานรับผิดชอบ : </font>" + _td.agencyname);
			item2.x = Config.GUI_SIZE.width / 2 - item2.width / 2;
			item2.y = item.y + item.height + 20;
			content.addChild(item2);

			var item3:HistoryItem = new HistoryItem("พิกัดทางภูมิศาสตร์", "<font color='#000000'>Latitude : </font>" + _td.latitude + "\n<font color='#000000'>Longitude : </font>" + _td.longtitude + "\n<font color='#000000'>ขนาดพื้นที่่ : </font>" + _td.space + "\n<font color='#000000'>รายละเอียดลักษณะเด่น : </font>" + _td.feature);
			item3.x = Config.GUI_SIZE.width / 2 - item3.width / 2;
			item3.y = item2.y + item2.height + 20;
			content.addChild(item3);

			var count:int = 0;
			var name_index:String;
			var tt:String = "";
			var dic:Dictionary = new Dictionary();
			for each (var ad:ActivityData in _td.activity)
			{
				count++;
				if (dic[ad.activity_type_name])
					dic[ad.activity_type_name] += ", " + ad.activityname;
				else
					dic[ad.activity_type_name] = ad.activityname;
					//tt += "<font color='#000000'>" + ad.activity_type_name + " : </font>" + ad.activityname + "\n";
			}
			for (name_index in dic)
			{
				count++;
				tt += "<font color='#000000'>" + name_index + " : </font>" + dic[name_index] + "\n";
			}
			var item4:HistoryItem = new HistoryItem("กิจกรรม", tt);
			item4.x = Config.GUI_SIZE.width / 2 - item4.width / 2;
			item4.y = item3.y + item3.height + 20;
			content.addChild(item4);

			tt = "";
			count = 0;
			for each (var fd:FacilitiesData in _td.facilities)
			{
				count++;
				tt += fd.facilitiesname; //"<font color='#000000'>" + fd.facilities_type_name + " : </font>" + fd.facilitiesname + "\n";
				if (count < _td.facilities.length)
					tt += ", ";
			}
			var item5:HistoryItem = new HistoryItem("สิ่งอำนวยความสะดวก", tt);
			item5.x = Config.GUI_SIZE.width / 2 - item5.width / 2;
			item5.y = item4.y + item4.height + 20;
			content.addChild(item5);


			tt = "";
			count = 0;
			dic = new Dictionary();
			for each (var ud:UtilityData in _td.utility)
			{
				count++;

				if (dic[ud.utility_type_name])
					dic[ud.utility_type_name] += ", " + ud.utilityname;
				else
					dic[ud.utility_type_name] = ud.utilityname
			}
			for (name_index in dic)
			{
				count++;
				tt += "<font color='#000000'>" + name_index + " : </font>" + dic[name_index] + "\n";
			}

			var item6:HistoryItem = new HistoryItem("ระบบสาธาณูปโภคพื้นฐาน", tt);
			item6.x = Config.GUI_SIZE.width / 2 - item6.width / 2;
			item6.y = item5.y + item5.height + 20;
			content.addChild(item6);
			var containerNormalViewport:Rectangle = new Rectangle(0, 0, 800, 1150);
			if (content.height > containerNormalViewport.height)
			{
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.verticalScrollingEnabled = true;
				_contentScroll.addScrollControll(content, _container, containerNormalViewport);
			}

			loadImage();
		}

		public function loadImage():void
		{
			var f:File;
			if (Config.isDownloadImageFromLocal)
				f = File.applicationDirectory.resolvePath(_td.imagepath);
			else
				f = File.documentsDirectory.resolvePath("dotr/" + _td.imagepath);

			_loader = LoaderUtil.loadBinaryAsBitmap(f.url, onComplete);
		}

		private function onComplete(event:Event):void
		{
			if (event.type != Event.COMPLETE)
				return;

			var src:Bitmap = _loader.contentLoaderInfo.content as Bitmap;
			var bitmapData:BitmapData = new BitmapData(_image.width, _image.height, false, 0x00FF00);
			var bitmap:Bitmap = new Bitmap(bitmapData);


			var ratioH:Number = bitmapData.height / src.height;
			var ratioV:Number = bitmapData.width / src.width;

			if (ratioV > ratioH)
				ratioH = ratioV;
			else
				ratioV = ratioH;

			var offsetX:Number = 0;
			var offsetY:Number = 0;

			bitmap.bitmapData.draw(src, new Matrix(ratioH, 0, 0, ratioV, offsetX, offsetY), null, null, null, true);

			_image.addChild(bitmap);
		}

		public function dispose():void
		{
			removeEventListener(MouseEvent.MOUSE_UP, onDown);
			destroy();
			completeSignal.dispatch();
			completeSignal.removeAll();
		}
	}
}


