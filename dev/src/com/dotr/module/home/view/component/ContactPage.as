package com.dotr.module.home.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;

	import assets.m00_contact_sp;

	public class ContactPage extends SDSprite
	{
		private var _skin:Sprite;

		public function ContactPage()
		{
			var rect:Rectangle;

			if (Config.isDynamic)
			{
				rect = Config.work_space;
			}
			else
			{
				rect = Config.GUI_SIZE;
			}

			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginFill(0xFDFBFC, 1);
			container.graphics.drawRect(0, 0, rect.width, rect.height);
			container.graphics.endFill();

			_skin = new assets.m00_contact_sp;

			if (Config.isDynamic)
				_skin.scaleX = _skin.scaleY = Config.ratio;

			_skin.x = rect.width / 2 - _skin.width / 2;
			addChild(_skin);
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);

		}

		public function dispose():void
		{
			destroy();
			_skin = null;
		}
	}
}
