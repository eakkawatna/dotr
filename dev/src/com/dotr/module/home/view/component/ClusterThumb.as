package com.dotr.module.home.view.component
{
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import assets.m00_thumb_cluster_sp;

	public class ClusterThumb extends SDSprite
	{
		private var _skin:Sprite;

		private var _loader:Loader;
		private var _path:String = "images/tour/4397/1.jpg";
		private var _content:Sprite;
		private var _id:String;
		private var _callBack:Function;

		public function ClusterThumb(id:String, name:String, total:String, index:int, path:String, callBack:Function)
		{
			_id = id;
			_path = path.replace("tour", "tour_thumb");
			_callBack = callBack;
			_skin = new assets.m00_thumb_cluster_sp;
			addChild(_skin);

			_skin.scrollRect = new Rectangle(0, 0, 217, 255);
			name_txt.text = name;
			total_landmark_txt.text = total;

			_content = new Sprite;
			image_sp.mask = mask_sp;
			image_sp.addChild(_content);
			index_txt.text = index.toString();

			loadImage();

			initHandler();

			cacheAsBitmap = true;
		}

		private function initHandler():void
		{
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			addEventListener(MouseEvent.MOUSE_OVER, onOver);
			addEventListener(MouseEvent.MOUSE_OUT, onOut);
			addEventListener(MouseEvent.MOUSE_UP, onUp);
		}

		protected function onOut(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				event.target.alpha = 1;
			}
		}

		protected function onOver(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				event.target.alpha = .5;
			}
		}


		protected function onRemove(event:Event):void
		{
			_id = null;
			_path = null;
			_callBack = null;

			removeEventListener(MouseEvent.MOUSE_UP, onUp);
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			removeEventListener(MouseEvent.MOUSE_OVER, onOver);
			removeEventListener(MouseEvent.MOUSE_OUT, onOut);
			removeEventListener(MouseEvent.MOUSE_UP, onUp);

			destroy();
		}

		protected function onUp(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				_callBack(_id);
			}
		}

		public function get id():String
		{
			return _id;
		}

		public function get index_txt():TextField
		{
			return _skin.getChildByName("index_txt") as TextField;
		}

		public function get click_btn():Sprite
		{
			return _skin.getChildByName("click_btn") as Sprite;
		}

		public function get name_txt():TextField
		{
			return _skin.getChildByName("name_txt") as TextField;
		}

		public function get total_landmark_txt():TextField
		{
			return _skin.getChildByName("total_landmark_txt") as TextField;
		}

		public function get image_sp():Sprite
		{
			return _skin.getChildByName("image_sp") as Sprite;
		}

		public function get mask_sp():Sprite
		{
			return _skin.getChildByName("mask_sp") as Sprite;
		}

		public function loadImage():void
		{
			var f:File;
			if (Config.isDownloadImageFromLocal)
				f = File.applicationDirectory.resolvePath(_path);
			else
				f = File.documentsDirectory.resolvePath("dotr/" + _path);
			ImageManager.loadImage("ClusterPage", f.url, onComplete);
		}

		private function onComplete(src:Bitmap):void
		{
			_content.addChild(src);
		}
	}
}
