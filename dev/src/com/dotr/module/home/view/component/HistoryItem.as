package com.dotr.module.home.view.component
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;

	import assets.m00_item_detial_sp;

	public class HistoryItem extends Sprite
	{
		private var _skin:Sprite;

		public function HistoryItem(head:String, detial:String)
		{
			_skin = new assets.m00_item_detial_sp;
			addChild(_skin);

			head_txt.text = head;
			detial_txt.width = 700;
			detial_txt.autoSize = TextFieldAutoSize.LEFT;
			detial_txt.htmlText = detial;
		}

		public function get head_txt():TextField
		{
			return _skin.getChildByName("head_txt") as TextField;
		}

		public function get detial_txt():TextField
		{
			return _skin.getChildByName("detial_txt") as TextField;
		}
	}
}
