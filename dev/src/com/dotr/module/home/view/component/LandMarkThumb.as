package com.dotr.module.home.view.component
{
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import assets.m00_thumb_landmark_sp;

	public class LandMarkThumb extends SDSprite
	{
		private var _skin:Sprite;
		private var _id:String;
		private var _content:Sprite;
		private var _path:String = "images/tour/no_image.png";
		private var _loader:Loader;

		public function LandMarkThumb(index:int, id:String, name:String, province:String, path:String)
		{
			_path = path.replace("tour", "tour_thumb");
			_skin = new assets.m00_thumb_landmark_sp;
			addChild(_skin);
			scrollRect = new Rectangle(0, 0, _skin.width, _skin.height);

			_id = id;

			name_txt.text = name;
			province_txt.text = !province ? "?" : province;

			_content = new Sprite;
			image_sp.mask = mask_sp;
			image_sp.addChild(_content);

			loadImage();
			index_txt.text = index.toString();
			initHandler();

			cacheAsBitmap = true;
		}

		private function initHandler():void
		{
			addEventListener(MouseEvent.MOUSE_OVER, onOver);
			addEventListener(MouseEvent.MOUSE_OUT, onOut);
		}

		protected function onOut(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				event.target.alpha = 1;
			}
		}

		protected function onOver(event:MouseEvent):void
		{
			if (event.target.name == "click_btn")
			{
				event.target.alpha = .5;
			}
		}

		public function get id():String
		{
			return _id;
		}

		public function get index_txt():TextField
		{
			return _skin.getChildByName("index_txt") as TextField;
		}

		public function get name_txt():TextField
		{
			return _skin.getChildByName("name_txt") as TextField;
		}

		public function get province_txt():TextField
		{
			return _skin.getChildByName("province_txt") as TextField;
		}

		public function get image_sp():Sprite
		{
			return _skin.getChildByName("image_sp") as Sprite;
		}

		public function get mask_sp():Sprite
		{
			return _skin.getChildByName("mask_sp") as Sprite;
		}

		public function loadImage():void
		{
			var f:File;
			if (Config.isDownloadImageFromLocal)
				f = File.applicationDirectory.resolvePath(_path);
			else
				f = File.documentsDirectory.resolvePath("dotr/" + _path);
			ImageManager.loadImage("LandMarkPage", f.url, onComplete);

		}

		private function onComplete(src:Bitmap):void
		{
			_content.addChild(src);
		}
	}
}
