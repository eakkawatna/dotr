package com.dotr.module.home.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.text.TextField;

	import assets.m00_thumb_name_sp;

	public class PageItem extends SDSprite
	{
		private var _skin:Sprite;
		private var _id:String;
		private var _start:int;
		private var _end:int;

		public function PageItem(id:String, start:int, end:int)
		{
			_id = id;
			_start = start;
			_end = end;
			_skin = new assets.m00_thumb_name_sp;
			addChild(_skin);

			title_tf.text = start + " - " + end;

			mouseChildren = false;
		}

		public function get end():int
		{
			return _end;
		}

		public function get start():int
		{
			return _start;
		}

		public function get id():String
		{
			return _id;
		}

		private function get title_tf():TextField
		{
			return _skin.getChildByName("title_tf") as TextField;
		}
	}
}
