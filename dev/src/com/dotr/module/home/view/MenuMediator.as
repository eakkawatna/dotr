package com.dotr.module.home.view
{
	import com.dotr.core.model.CoreModel;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class MenuMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:MenuView;

		// create ------------------------------------------------------------------

		override public function onRegister():void
		{
			module.create();
			initMouseHandler();
		}

		// event ------------------------------------------------------------------
		private function initMouseHandler():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			if (module.isSlide)
				return;

			var targetName:String = event.target.name;
			switch (targetName)
			{
				case "map_btn":
					module.selectMenuSignal.dispatch(MenuView.MAP);
					break;
				case "cluster_btn":
					module.selectMenuSignal.dispatch(MenuView.CLUSTER);
					break;
				case "contact_btn":
					module.selectMenuSignal.dispatch(MenuView.CONTACT);
					break;
				case "admin_btn":
					module.selectMenuSignal.dispatch(MenuView.ADMIN);
					break;
				case "ads_1_btn":
					navigateToURL(new URLRequest("http://www.homestaythai.net/"), "_blank");
					break;
				case "ads_2_btn":
					navigateToURL(new URLRequest("http://61.19.236.142/hotspring/"), "_blank");
					break;
				case "ads_3_btn":
					navigateToURL(new URLRequest("http://www.longstayatthailand.com/home/index.php"), "_blank");
					break;
				case "ads_4_btn":
					navigateToURL(new URLRequest("http://thaits.org/tts_pr/"), "_blank");
					break;
				default:
					module.slide(function():void
					{
					});
			}
		}


		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);

			appModel = null;
			module = null;
		}
	}
}
