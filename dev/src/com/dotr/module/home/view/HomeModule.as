package com.dotr.module.home.view
{
	import com.dotr.core.model.data.ClusterData;
	import com.dotr.core.model.data.ProvinceData;
	import com.dotr.core.model.data.RegionData;
	import com.dotr.core.model.data.TouristData;
	import com.dotr.module.home.HomeContext;
	import com.dotr.module.home.view.component.AdvanceSearchPage;
	import com.dotr.module.home.view.component.ClusterPage;
	import com.dotr.module.home.view.component.ContactPage;
	import com.dotr.module.home.view.component.DetailPage;
	import com.dotr.module.home.view.component.LandMarkPage;
	import com.dotr.module.home.view.component.MapPage;
	import com.dotr.module.home.view.component.PreloadComponent;
	import com.dotr.module.home.view.component.ProvincePage;
	import com.dotr.module.home.view.component.SelectPage;
	import com.dotr.module.home.view.component.SelectTypePage;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.system.System;

	import __AS3__.vec.Vector;

	import assets.m00_bg_bmp;

	import org.osflash.signals.Signal;
	import org.robotlegs.core.IInjector;


	public class HomeModule extends ModuleBase
	{
		public static const PROVINCE:String = "province";
		public static const CLUSTER:String = "cluster";
		public static const MAP:String = "map";
		public static const LANDMARK:String = "landmark";
		public static const CONTACT:String = "contact";
		public static const SEARCH:String = "search";

		public var mapSignal:Signal = new Signal(String);
		public var provinceSignal:Signal = new Signal(String);
		public var landmarkSignal:Signal = new Signal(String);
		public var clusterSignal:Signal = new Signal(String);
		public var selectTypeSignal:Signal = new Signal(String);
		public var touristDatas:Vector.<TouristData>;

		private var _page:Sprite;
		private var _menu:MenuView;
		private var _actionbar:ActionBarView;
		private var _contianer:Sprite;
		private var _isTween:Boolean;
		private var _bp:Bitmap;
		private var _search:SearchView;
		private var _cluster:ClusterPage;
		private var _province:ProvincePage;
		private var _landmark:LandMarkPage;
		private var _currentLandmark:Vector.<TouristData>;
		private var _currentClusterDatas:Vector.<ClusterData>;
		private var _currentProvinces:Vector.<ProvinceData>;
		private var _currentPage:String = MAP;
		private var _prev_page:String;
		private var _contact:ContactPage;
		private var _content:Sprite;
		private var _total_tourism:int;

		private var _detail:DetailPage;
		private var _currentLandMarkDatas:Vector.<TouristData>;
		private var _title_province:String;
		private var _title_landmark:String;
		private var _block:Boolean;
		private var _dialog:PreloadComponent;
		private var _select_menu:String;
		private var _regionDatas:Vector.<RegionData>;
		private var _start:int = 1;
		private var _end:int = 99;
		private var _bg:Bitmap;
		private var _top_layer:Sprite;

		public function get total_tourism():int
		{
			return _total_tourism;
		}

		public function set total_tourism(value:int):void
		{
			_total_tourism = value;
		}

		public function get currentLandMarkDatas():Vector.<TouristData>
		{
			return _currentLandMarkDatas;
		}

		public function set currentLandMarkDatas(value:Vector.<TouristData>):void
		{
			_currentLandMarkDatas = value;
		}

		public function get currentProvinces():Vector.<ProvinceData>
		{
			return _currentProvinces;
		}

		public function set currentProvinces(value:Vector.<ProvinceData>):void
		{
			_currentProvinces = value;
		}

		public function get currentClusterDatas():Vector.<ClusterData>
		{
			return _currentClusterDatas;
		}

		public function set currentClusterDatas(value:Vector.<ClusterData>):void
		{
			_currentClusterDatas = value;
		}

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new HomeContext(this, value);
		}

		override public function create():void
		{
			var rect:Rectangle;
			if (Config.isDynamic)
			{
				rect = Config.work_space;
			}
			else
			{
				rect = Config.GUI_SIZE;
			}

			// add background ---------
			_bg = new Bitmap(new m00_bg_bmp);
			_bg.width = rect.width;
			_bg.height = rect.height;
			addChild(_bg);

			// setup container ---------
			_contianer = new Sprite();
			_contianer.graphics.beginFill(0x007FFF, 0);
			_contianer.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			_contianer.graphics.endFill();

			if (Config.isDynamic)
				_contianer.scaleX = _contianer.scaleY = Config.ratio;

			_contianer.x = rect.width / 2 - _contianer.width / 2;
			addChild(_contianer);

			// add page container ---------
			_page = new Sprite();
			_page.x = 50;
			_page.y = 362;
			_contianer.addChild(_page);

			// add search ---------
			_search = new SearchView();
			_search.x = 50;
			_search.y = 124;
			_search.selectProvinceSignal.add(allProvince);
			_search.addAdvance.add(onAddAdvance);
			_contianer.addChild(_search);

			// add search ---------
			_content = new Sprite();
			_content.y = 80;
			_contianer.addChild(_content);

			// add action bar container ---------
			_actionbar = new ActionBarView();
			addChild(_actionbar);

			_actionbar.slideContainerSignal.add(slideContainer);
			_actionbar.backSignal.add(onBack);

			completeSignal.dispatch("");

			// setup container ---------
			_top_layer = new Sprite();
			addChild(_top_layer);
			//menu
			_menu = new MenuView();
			_menu.selectMenuSignal.add(onSelectMenu);
			addChild(_menu);

		}

		private function onAddAdvance(asp:AdvanceSearchPage):void
		{
			addChild(asp);
		}

		private function allProvince():void
		{
			mapSignal.dispatch('*');
		}

		public function onBack():void
		{
			if (_detail)
			{
				_detail.completeSignal.dispatch();
			}

			switch (_currentPage)
			{
				case PROVINCE:
					if (_prev_page == CLUSTER)
						createCluster(_currentClusterDatas);
					else
						createMap(_regionDatas);

					break;
				case LANDMARK:
					//back to Province
					if (_prev_page == PROVINCE)
					{
						createProvince(_title_province);
					}
					else if (_prev_page == CLUSTER)
					{
						createCluster(_currentClusterDatas);
					}
					else if (_prev_page == LANDMARK)
					{
						selectTypeSignal.dispatch("all");
					}
					else
						createMap(_regionDatas);
					break;

				case SEARCH:
					//back to Province
					if (_prev_page == PROVINCE)
					{
						createProvince(_title_province);
					}
					else if (_prev_page == CLUSTER)
					{
						createCluster(_currentClusterDatas);
					}
					else
						createMap(_regionDatas);
					_search.search_txt.text = "";
					break;
				case CONTACT:
					if (_prev_page == PROVINCE)
					{
						createProvince(_title_province);
					}
					else if (_prev_page == CLUSTER)
					{
						createCluster(_currentClusterDatas);
					}
					else
						createMap(_regionDatas);
					break;
			}
		}

		private function onSelectMenu(value:String):void
		{
			if (_isTween)
				return;

			_select_menu = value;
			slideContainer();
		}

		public function createContact():void
		{
			// remove
			removeContent();
			//create
			_currentPage = CONTACT;
			_actionbar.title = "CONTACT US";
			_actionbar.back = true;

			_contact = new ContactPage();
			_contact.y = _actionbar.height;
			_top_layer.addChild(_contact);

			// complete
			addComplete();
		}

		public function createCluster(datas:Vector.<ClusterData>):void
		{
			// remove
			removeContent();
			//create
			setTypeSearchText("");

			_search.title_txt.text = "จำนวนแหล่งท่องเที่ยวทั้งหมด " + total_tourism + " แหล่ง";

			// log page
			if (_currentPage)
				_prev_page = _currentPage;
			_currentPage = CLUSTER;

			_actionbar.title = "กรมการท่องเที่ยว";
			_actionbar.back = false;
			_currentClusterDatas = datas;

			_cluster = new ClusterPage(_currentClusterDatas, touristDatas);
			_cluster.selectSignal.add(clusterSignal.dispatch);
			_page.addChild(_cluster);

			// complete
			addComplete();
		}

		public function createMap(regionDatas:Vector.<RegionData>):void
		{
			// remove
			removeContent();
			//create
			setTypeSearchText("");

			_regionDatas = regionDatas;
			_search.title_txt.text = "จำนวนแหล่งท่องเที่ยวทั้งหมด " + total_tourism + " แหล่ง";

			if (_currentPage)
				_prev_page = _currentPage;
			_currentPage = MAP;

			_actionbar.title = "กรมการท่องเที่ยว";
			_actionbar.back = false;

			var map:MapPage = new MapPage(regionDatas);
			map.selectSignal.add(mapSignal.dispatch);
			_page.addChild(map);

			// complete
			addComplete();
		}

		private function removeContent():void
		{
			// remove all item in _page
			if (_page)
				DisplayObjectUtil.removeChildren(_page);

			if (_contact)
			{
				_top_layer.removeChild(_contact);
				_contact.dispose();
				_contact = null;
			}

			if (_cluster)
				_cluster = null;
			if (_province)
				_province = null;
			if (_landmark)
				_landmark = null;

			block = true;
		}

		private function addComplete():void
		{
			_start = 1;
			_end = 99;
			System.gc();
			block = false;
		}

		public function createProvince(title:String = null):void
		{
			// remove
			removeContent();
			//create

			setTypeSearchText("");

			if (_currentPage)
				_prev_page = _currentPage;
			_currentPage = PROVINCE;

			_actionbar.back = true;
			_actionbar.title = !title ? "จังหวัด" : title;
			_title_province = title;

			var total:int = 0;
			for each (var item:ProvinceData in _currentProvinces)
				total += item.pv_total;
			_search.title_txt.text = "จำนวนแหล่งท่องเที่ยวทั้งหมด " + total + " แหล่ง";

			_province = new ProvincePage(_currentProvinces, touristDatas);
			_province.selectSignal.add(provinceSignal.dispatch);
			_page.addChild(_province);

			addComplete();
		}

		public function createLandmark(datas:Vector.<TouristData>, title:String, type:String = null):void
		{
			_actionbar.back = true;
			_actionbar.title = !title ? "สถานที่ท่องเที่ยว" : title;
			_title_landmark = title;
			_currentLandmark = datas;

			if (_currentPage)
				_prev_page = _currentPage;
			if (type == "all")
				_prev_page = PROVINCE;
			_currentPage = LANDMARK;

			removeContent();

			var new_data:Vector.<TouristData> = new Vector.<TouristData>;
			if (datas.length > 0 && datas.length >= _end)
				for (var i:int = _start - 1; i < _end; i++)
				{
					new_data.push(datas[i]);
				}
			else
				new_data = datas;

			_landmark = new LandMarkPage(new_data, _start, datas.length <= 99);
			_landmark.selectSignal.add(landmarkSignal.dispatch);
			_page.addChild(_landmark);

			addComplete();

		}

		public function createSearch(datas:Vector.<TouristData>):void
		{
			_actionbar.back = true;
			_actionbar.title = "สถานที่ท่องเที่ยว";
			_currentLandmark = datas;

			if (_currentPage)
				_prev_page = _currentPage;
			_currentPage = SEARCH;

			removeContent();

			var new_data:Vector.<TouristData> = new Vector.<TouristData>;
			if (datas.length > 0 && datas.length >= _end)
				for (var i:int = _start - 1; i < _end; i++)
				{
					new_data.push(datas[i]);
				}
			else
				new_data = datas;

			_landmark = new LandMarkPage(new_data, _start, datas.length <= 99);
			_landmark.selectSignal.add(landmarkSignal.dispatch);
			_page.addChild(_landmark);

			addComplete();
		}

		public function createDetail(id:String):void
		{
			var td:TouristData = VectorUtil.getItemBy(_currentLandmark, id, "touristid");
			if (td)
			{
				_detail = new DetailPage(td);
				_detail.completeSignal.addOnce(detialComplete)
				addChild(_detail);

				_actionbar.status = false;
			}
		}

		private function detialComplete():void
		{
			_detail = null;
			_actionbar.status = true;
		}

		public function createSelectType():void
		{
			var selectTypePage:SelectTypePage = new SelectTypePage();
			selectTypePage.selectSignal.addOnce(selectTypeSignal.dispatch);
			addChild(selectTypePage);
		}

		public function createSelectPage():void
		{
			var selectPage:SelectPage = new SelectPage(_currentLandmark.length);
			selectPage.selectSignal.addOnce(onPage);
			addChild(selectPage);
		}

		private function onPage(start:int, end:int):void
		{
			_start = start;
			_end = end;
			createLandmark(_currentLandmark, _title_landmark, "all");

		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			super.deactivate(onDeactivated, nextView);
		}

		public function slideContainer():void
		{
			if (_isTween)
				return;

			_isTween = true;
			_actionbar.status = false;
			_menu.slide(slideComplete);
		}

		private function slideComplete():void
		{
			_actionbar.status = true;
			_isTween = false;

			switch (_select_menu)
			{
				case MenuView.MAP:
					createMap(_regionDatas);
					break;
				case MenuView.CLUSTER:
					createCluster(_currentClusterDatas);
					break;
				case MenuView.CONTACT:
					createContact();
					break;
			}
		}

		public function get block():Boolean
		{
			return _block;
		}

		public function set block(value:Boolean):void
		{
			if (_block == value)
				return;

			_block = value;

			if (_block)
				addChild(_dialog = new PreloadComponent)
			else
			{
				if (_dialog)
					_dialog.dispose();
			}
		}

		override public function dispose():void
		{
			if (_actionbar)
				_actionbar.slideContainerSignal.removeAll();
			destroy();
			super.dispose();
		}

		public function setTypeSearchText(tourist_typename:String):void
		{
			try
			{
				if (tourist_typename != "")
					_search.title_2_txt.text = "ประเภทแหล่งท่องเที่ยว : " + tourist_typename;
				else
					_search.title_2_txt.text = "";
			}
			catch (e:Error)
			{
			}
		}
	}
}


