package com.dotr.module.home.view
{
	import com.dotr.core.model.CoreModel;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class ActionBarMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:ActionBarView;

		// create ------------------------------------------------------------------

		override public function onRegister():void
		{
			module.create();
			initMouseHandler();

			module.back = false;
		}

		// event ------------------------------------------------------------------
		private function initMouseHandler():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseDown);
		}

		private function onMouseDown(event:MouseEvent):void
		{
			if (!module.status)
				return;

			var targetName:String = event.target.name;
			switch (targetName)
			{
				case "menu_btn":
					module.slideContainerSignal.dispatch();
					break;
				case "back_btn":
					module.backSignal.dispatch();
					break;
			}
		}

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);

			appModel = null;
			module = null;
		}
	}
}
