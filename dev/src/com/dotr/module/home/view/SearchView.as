package com.dotr.module.home.view
{
	import com.dotr.module.home.view.component.AdvanceSearchPage;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.view.ViewBase;
	
	import flash.display.Sprite;
	import flash.text.TextField;
	
	import assets.m00_search_sp;
	
	import org.osflash.signals.Signal;


	public class SearchView extends ViewBase
	{
		public var selectProvinceSignal:Signal = new Signal();
		public var addAdvance:Signal = new Signal(AdvanceSearchPage);
		
		private var _skin:Sprite;

		public function create():void
		{
			_skin = new assets.m00_search_sp;
			addChild(_skin);
		}

		public function get search_txt():TextField
		{
			return _skin.getChildByName("search_txt") as TextField;
		}

		public function get title_txt():TextField
		{
			return _skin.getChildByName("title_txt") as TextField;
		}

		public function get title_2_txt():TextField
		{
			return _skin.getChildByName("title_2_txt") as TextField;
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

	}
}
