package com.dotr.module.home.view
{
	import com.dotr.core.model.CoreModel;
	import com.dotr.core.model.data.TouristData;
	import com.dotr.module.home.view.component.AdvanceSearchPage;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class SearchMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var view:SearchView;

		// create ------------------------------------------------------------------
		private var _advance:AdvanceSearchPage;
		private var _searchDatas:Vector.<TouristData>;

		override public function onRegister():void
		{
			coreModel.changeTotalSignal.add(onChangeTotal);
			view.create();
			initMouseHandler();

			onChangeTotal();

		}

		private function onChangeTotal():void
		{
			view.title_txt.text = "จำนวนแหล่งท่องเที่ยวทั้งหมด " + coreModel.tour_total + " แหล่ง";
		}

		// event ------------------------------------------------------------------
		private function initMouseHandler():void
		{
			eventMap.mapListener(view, MouseEvent.MOUSE_DOWN, onMouseDown);
		}

		private function onMouseDown(event:MouseEvent):void
		{
			if (event.target == view.search_txt)
				view.search_txt.text = "";

			switch (event.target.name)
			{
				case "search_btn":

					var obj:Object = new Object();
					obj.cluster_id = "";
					obj.region_id = "";
					obj.province_id = "";
					obj.amphur_id = "";
					obj.subDistrict_id = "";
					obj.tourist_type_id = "";
					obj.sub_tourist_type_id = "";
					obj.keyword = view.search_txt.text;

					if (obj.keyword != "" && obj.keyword != " " && obj.keyword != "ชื่อสถานที่ท่องเที่ยว")
						onSearch(obj);
					else
						view.selectProvinceSignal.dispatch();

					break;

				case "advance_search_btn":
					_advance = new AdvanceSearchPage(coreModel.regionDatas, coreModel.clusterDatas, coreModel.provinceDatas, coreModel.amphurDatas, coreModel.subDistrictDatas, coreModel.touristTypeDatas, coreModel.subTouristTypeDatas);
					_advance.searchSignal.addOnce(onSearch);
					view.addAdvance.dispatch(_advance);
					break;

			}
		}

		private function onSearch(data:Object):void
		{
			_searchDatas = coreModel.touristDatas.concat();
			var collectDatas:Vector.<TouristData> = _searchDatas.concat();
			//
			if (data.cluster_id != "")
				_searchDatas = search(collectDatas, data.cluster_id, "clusterid");
			collectDatas = _searchDatas.concat();
			//
			if (data.region_id != "")
				_searchDatas = search(collectDatas, data.region_id, "region_id");
			collectDatas = _searchDatas.concat();
			//
			if (data.province_id != "")
				_searchDatas = search(collectDatas, data.province_id, "provinceid");
			collectDatas = _searchDatas.concat();
			//
			if (data.amphur_id != "")
				_searchDatas = search(collectDatas, data.amphur_id, "amphorid");
			collectDatas = _searchDatas.concat();
			//
			if (data.subDistrict_id != "")
				_searchDatas = search(collectDatas, data.subDistrict_id, "districtid");
			collectDatas = _searchDatas.concat();
			//
			if (data.tourist_type_id != "")
				_searchDatas = search(collectDatas, data.tourist_type_id, "tourist_typeid");
			collectDatas = _searchDatas.concat();
			//
			if (data.sub_tourist_type_id != "")
				_searchDatas = search(collectDatas, data.sub_tourist_type_id, "tourist_subtypeid");
			collectDatas = _searchDatas.concat();

			if (data.keyword != "")
				_searchDatas = searchKeyword(collectDatas, data.keyword);
			collectDatas = _searchDatas.concat();

			coreModel.searchDatas = _searchDatas;
		}

		private function search(vector:*, key:*, arg:String = "id"):Vector.<TouristData>
		{
			var datas:Vector.<TouristData> = new Vector.<TouristData>();
			for each (var _item:* in vector)
				if (_item[arg] == key)
					datas.push(_item);
			return datas;
		}

		private function searchKeyword(vector:*, key:*):Vector.<TouristData>
		{
			var datas:Vector.<TouristData> = new Vector.<TouristData>();
			for each (var _item:* in vector)
				if (String(_item["keyword"]).indexOf(key) != -1)
					datas.push(_item);
			return datas;
		}

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(view.ID);

			appModel = null;
			view = null;
		}
	}
}
