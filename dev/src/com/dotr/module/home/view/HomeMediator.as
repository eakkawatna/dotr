package com.dotr.module.home.view
{
	import com.dotr.core.model.CoreModel;
	import com.dotr.core.model.data.ClusterData;
	import com.dotr.core.model.data.ProvinceData;
	import com.dotr.core.model.data.RegionData;
	import com.dotr.core.model.data.TouristData;
	import com.dotr.core.model.data.TouristTypeData;
	import com.greensock.TweenLite;
	import com.idealizm.manager.ImageManager;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.sleepydesign.utils.VectorUtil;

	import flash.desktop.NativeApplication;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class HomeMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:HomeModule;

		// create ------------------------------------------------------------------

		override public function onRegister():void
		{
			ImageManager.loadingSignal.add(onLoading);
			coreModel.searchSignal.add(onSearch);

			// add Signal
			module.completeSignal.addOnce(onComplete);
			module.provinceSignal.add(onSelectProvince);
			module.landmarkSignal.add(onSelectLandmark);
			module.mapSignal.add(onSelectMap);
			module.clusterSignal.add(onSelectCluster);
			module.selectTypeSignal.add(onSelectType);
			module.create();

			//set 
			module.total_tourism = coreModel.base_tour_total;
			initMouseHandler();
		}

		private function onLoading(value:Boolean):void
		{
			if (module.block != value)
				module.block = value;
		}

		private function onSelectType(id:String):void
		{
			module.block = true;
			delay(function():void
			{
				var datas:Vector.<TouristData> = new Vector.<TouristData>();

				if (id == "")
					return;

				if (id == "all")
				{
					module.createLandmark(module.currentLandMarkDatas, null, "all");
					coreModel.tour_total = module.currentLandMarkDatas.length;
					module.setTypeSearchText("ทุกประเภท");
					return;
				}

				for each (var item:TouristData in module.currentLandMarkDatas)
					if (item.tourist_typeid == id)
						datas.push(item);

				var ttd:TouristTypeData = VectorUtil.getItemBy(coreModel.touristTypeDatas, id, "tourist_typeid");

				module.setTypeSearchText(ttd.tourist_typename);
				module.createLandmark(datas, null);
				coreModel.tour_total = datas.length;
			});
		}

		private function onComplete(value:String):void
		{
			module.block = true;
			delay(function():void
			{
				module.currentClusterDatas = coreModel.clusterDatas;
				module.touristDatas = coreModel.touristDatas;

				coreModel.tour_total = coreModel.base_tour_total;
				module.createMap(coreModel.regionDatas);
			});
		}

		private function onSearch():void
		{
			module.block = true;
			delay(function():void
			{
				coreModel.tour_total = coreModel.searchDatas.length;
				module.currentLandMarkDatas = coreModel.searchDatas;
				module.createSearch(coreModel.searchDatas);
			});
		}

		private function onSelectLandmark(value:String):void
		{
			if (value == "type")
				module.createSelectType();
			else if (value == "page")
			{
				module.createSelectPage();
			}
			else
				module.createDetail(value);
		}

		private function onSelectMap(id:String):void
		{
			module.block = true;

			delay(function():void
			{
				var datas:Vector.<ProvinceData> = new Vector.<ProvinceData>();

				if (id == '*')
				{
					datas = coreModel.provinceDatas;
					module.currentProvinces = datas;
					module.createProvince("ข้อมูลทุกภาค (" + datas.length + ")");
				}
				else
				{
					for (var i:int = 0; i < coreModel.provinceDatas.length; i++)
					{
						var item:ProvinceData = coreModel.provinceDatas[i];
						if (item.region_id == id)
							datas.push(item);
					}

					var region:RegionData = VectorUtil.getItemBy(coreModel.regionDatas, id, "region_id");
					module.currentProvinces = datas;
					module.createProvince(region.region_name + "(" + region.region_total + ")");
				}

			/*var total:int = 0;
			for each (item in datas)
				total += item.pv_total;

			coreModel.tour_total = total;*/
			});

		}

		private function onSelectProvince(id:String):void
		{
			module.block = true;

			delay(function():void
			{
				var datas:Vector.<TouristData> = new Vector.<TouristData>();

				for each (var item:TouristData in coreModel.touristDatas)
					if (item.provinceid == id)
						datas.push(item);

				var province:ProvinceData = VectorUtil.getItemBy(coreModel.provinceDatas, id, "province_id");
				module.currentLandMarkDatas = datas;
				module.createLandmark(datas, province.province_name + "(" + datas.length + ")");
				module.setTypeSearchText("ทุกประเภท");
				coreModel.tour_total = datas.length;
			});
		}

		private function onSelectCluster(id:String):void
		{
			module.block = true;

			delay(function():void
			{
				var datas:Vector.<TouristData> = new Vector.<TouristData>();

				for each (var item:TouristData in coreModel.touristDatas)
					if (item.clusterid == id)
						datas.push(item);

				var cluster:ClusterData = VectorUtil.getItemBy(coreModel.clusterDatas, id, "clusterid");
				module.currentLandMarkDatas = datas;
				module.createLandmark(datas, cluster.clustername + "(" + cluster.cluster_total + ")");
				module.setTypeSearchText("ทุกประเภท");
				coreModel.tour_total = datas.length;
			});

		}

		private function delay(complete:Function):void
		{
			TweenLite.to(module, 0.5, {onComplete: complete})
		}

		// event ------------------------------------------------------------------
		private function initMouseHandler():void
		{
			NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true)
		}

		private function onKeyDown(event:KeyboardEvent):void
		{
			switch (event.keyCode)
			{
				case Keyboard.BACK:
				{
					// don't let the OS kill focus to our app
					module.onBack();
					event.preventDefault();
					event.stopImmediatePropagation();
					break;
				}
			}

		}

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);

			appModel = null;
			module = null;
		}
	}
}


