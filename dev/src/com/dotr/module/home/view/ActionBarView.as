package com.dotr.module.home.view
{
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.view.ViewBase;

	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import ActionBar.button_back;
	import ActionBar.button_menu;

	import assets.m00_action_bar_sp;

	import org.osflash.signals.Signal;


	public class ActionBarView extends ViewBase
	{
		public var slideContainerSignal:Signal = new Signal;
		public var backSignal:Signal = new Signal;
		public var HEIGHT:int = 84;
		private var _status:Boolean = true;
		private var _skin:Sprite;
		private var _back_btn:Sprite;
		private var _menu_btn:button_menu;


		public function ActionBarView()
		{
			var rect:Rectangle;

			// skin
			_skin = new assets.m00_action_bar_sp;

			//back_btn
			_back_btn = new ActionBar.button_back;
			_back_btn.name = "back_btn";

			//menu_btn
			_menu_btn = new ActionBar.button_menu;
			_menu_btn.name = "menu_btn";

			if (Config.isDynamic)
			{
				HEIGHT *= Config.ratio;
				rect = Config.work_space;
				_skin.scaleX = _skin.scaleY = Config.ratio;
				_back_btn.scaleX = _back_btn.scaleY = Config.ratio;
				_menu_btn.scaleX = _menu_btn.scaleY = Config.ratio;
			}
			else
			{
				rect = Config.GUI_SIZE;
			}

			graphics.beginFill(0x007FFF, 1);
			graphics.drawRect(0, 0, rect.width, HEIGHT);
			graphics.endFill();

			_back_btn.x = rect.width - back_btn.width;
			_skin.x = rect.width / 2 - _skin.width / 2;
			_skin.y = HEIGHT / 2 - _skin.height / 2;


		}

		public function get status():Boolean
		{
			return _status;
		}

		public function set status(value:Boolean):void
		{
			_status = value;

			if (_status)
			{
				mouseEnabled = true;
				mouseChildren = true;
			}
			else
			{
				mouseEnabled = false;
				mouseChildren = false;
			}
		}

		public function create():void
		{
			addChild(_skin);
			addChild(_back_btn);
			addChild(_menu_btn);
			title = "กรมการท่องเที่ยว";
		}

		public function get title_txt():TextField
		{
			return _skin.getChildByName("title_txt") as TextField;
		}

		public function get back_btn():Sprite
		{
			return _back_btn;
		}

		public function set back(value:Boolean):void
		{
			back_btn.visible = value;
			back_btn.mouseEnabled = value;
		}

		public function set title(value:String):void
		{
			if (_skin)
				title_txt.text = value;
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			//super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			//super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

	}
}
