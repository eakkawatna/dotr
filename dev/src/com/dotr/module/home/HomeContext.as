package com.dotr.module.home
{

	import com.dotr.module.home.view.ActionBarMediator;
	import com.dotr.module.home.view.ActionBarView;
	import com.dotr.module.home.view.HomeMediator;
	import com.dotr.module.home.view.HomeModule;
	import com.dotr.module.home.view.MenuMediator;
	import com.dotr.module.home.view.MenuView;
	import com.dotr.module.home.view.SearchMediator;
	import com.dotr.module.home.view.SearchView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class HomeContext extends ModuleContext
	{
		public function HomeContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			mediatorMap.mapView(HomeModule, HomeMediator);
			mediatorMap.mapView(MenuView, MenuMediator);
			mediatorMap.mapView(ActionBarView, ActionBarMediator);
			mediatorMap.mapView(SearchView, SearchMediator);

			super.startup();
		}

		override public function dispose():void
		{
			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);



			super.dispose();
		}
	}
}
