package com.dotr.core.controller
{
	import com.dotr.core.remote.service.CoreService;

	import org.robotlegs.mvcs.SignalCommand;

	public class RequestSaveLocalDataCommand extends SignalCommand
	{
		[Inject]
		public var service:CoreService;

		[Inject(name = 'raw')]
		public var raw:String;

		[Inject(name = 'file_name')]
		public var file_name:String;

		//-get carees data from server.
		override public function execute():void
		{
			service.saveFile(raw, file_name);

			super.execute();
		}
	}
}
