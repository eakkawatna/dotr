package com.dotr.core.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import org.osflash.signals.Signal;

	public class DialogCameraComponent extends SDSprite
	{
		public static const CAMERA_UI:String = "take_photo_btn";
		public static const CAMERA_ROLL:String = "album_photo_btn";
		public static const CLOSE:String = "close_btn";

		private var _skin:Sprite;
		private var _onClickSignal:Signal;

		/**
		 *
		 * @param title
		 * @param desc
		 * @param type
		 * @param onClickSignal (String)
		 *
		 */
		public function DialogCameraComponent(onClickSignal:Signal = null)
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);

			_onClickSignal = onClickSignal;
		}

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);

			create();

			if (_onClickSignal)
				addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		protected function onMouseUp(event:MouseEvent):void
		{
			switch (event.target.name)
			{
				case CLOSE:
				{
					dispose();
					break;
				}
				case CAMERA_ROLL:
				{
					_onClickSignal.dispatch(CAMERA_ROLL);
					dispose();
					break;
				}
				case CAMERA_UI:
				{
					_onClickSignal.dispatch(CAMERA_UI);
					dispose();
					break;
				}

				default:
				{
					break;
				}
			}

		}

		private function create():void
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			_skin = new Dialog.select_camera_sp;
			_skin.x = Config.GUI_SIZE.width / 2;
			_skin.y = Config.GUI_SIZE.height / 2;
			addChild(_skin);

		}


		//destroy ---------------------------------------------

		public function dispose():void
		{
			removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			_onClickSignal.removeAll();
			_onClickSignal = null;
			destroy();
		}
	}
}
