package com.dotr.core.model.data
{

	public class ClusterData extends Object
	{
		private var _clusterid:String;
		private var _clustername:String;
		private var _cluster_total:int = 0;

		public function ClusterData(clusterid:String, clustername:String, cluster_total:int)
		{
			_clusterid = clusterid;
			_clustername = clustername;
			_cluster_total = cluster_total;
		}

		public function get cluster_total():int
		{
			return _cluster_total;
		}

		public function get clusterid():String
		{
			return _clusterid;
		}

		public function get clustername():String
		{
			return _clustername;
		}



	}
}
