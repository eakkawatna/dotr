package com.dotr.core.model.data
{

	public class FacilitiesData extends Object
	{
		private var _facilitiesid:String;
		private var _facilitiesname:String;
		private var _facilitiestypeid:String;
		private var _facilities_type_name:String;

		public function FacilitiesData(facilitiesid:String, facilitiesname:String, facilitiestypeid:String, facilities_type_name:String)
		{
			_facilitiesid = facilitiesid;
			_facilitiesname = facilitiesname;
			_facilitiestypeid = facilitiestypeid;
			_facilities_type_name = facilities_type_name;

		}

		public function get facilities_type_name():String
		{
			return _facilities_type_name;
		}

		public function get facilitiestypeid():String
		{
			return _facilitiestypeid;
		}

		public function get facilitiesname():String
		{
			return _facilitiesname;
		}

		public function get facilitiesid():String
		{
			return _facilitiesid;
		}

	}
}
