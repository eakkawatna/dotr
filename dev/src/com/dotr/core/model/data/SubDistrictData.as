package com.dotr.core.model.data
{

	public class SubDistrictData extends Object
	{
		private var _district_id:String;
		private var _district_name:String;
		private var _amphur_id:String;
		private var _province_id:String;

		public function SubDistrictData(district_id:String, district_name:String, amphur_id:String, province_id:String)
		{
			_district_id = district_id;
			_district_name = district_name;
			_amphur_id = amphur_id;
			_province_id = province_id;
		}

		public function get province_id():String
		{
			return _province_id;
		}

		public function get amphur_id():String
		{
			return _amphur_id;
		}

		public function get district_name():String
		{
			return _district_name;
		}

		public function get district_id():String
		{
			return _district_id;
		}

	}
}
