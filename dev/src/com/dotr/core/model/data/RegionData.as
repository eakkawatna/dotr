package com.dotr.core.model.data
{

	public class RegionData extends Object
	{
		private var _region_id:String;
		private var _region_name:String;
		private var _region_total:int = 0;

		public function RegionData(region_id:String, region_name:String, region_total:int = 0)
		{
			_region_id = region_id;
			_region_name = region_name;
			_region_total = region_total;

		}

		public function get region_total():int
		{
			return _region_total;
		}

		public function get region_name():String
		{
			return _region_name;
		}

		public function get region_id():String
		{
			return _region_id;
		}

	}
}
