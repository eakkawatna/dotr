package com.dotr.core.model.data
{

	public class TouristData extends Object
	{
		private var _touristid:String;
		private var _tourist_typeid:String;
		private var _tourist_subtypeid:String;
		private var _touristcode:String;
		private var _tourist_pic:String;
		private var _touristname:String;
		private var _clusterid:String;
		private var _region_id:String;
		private var _provinceid:String;
		private var _amphorid:String;
		private var _districtid:String;
		private var _postcode:String;
		private var _latitude:String;
		private var _longtitude:String;
		private var _agencyid:String;
		private var _address:String;
		private var _website:String;
		private var _email:String;
		private var _space:String;
		private var _feature:String;
		private var _keyword:String;
		private var _telnumber:String;
		private var _date_open:String;
		private var _description:String;
		private var _counter_thai:String;
		private var _counter_visitor:String;
		private var _status:String;
		private var _tourist_typename:String;
		private var _province_name:String;
		private var _amphur_name:String;
		private var _district_name:String;
		private var _agencyname:String;
		private var _activity:Vector.<ActivityData>;
		private var _facilities:Vector.<FacilitiesData>;
		private var _utility:Vector.<UtilityData>;
		private var _imagepath:String;

		public function TouristData(touristid:String, tourist_typeid:String, tourist_subtypeid:String, touristcode:String, tourist_pic:String, touristname:String, clusterid:String, region_id:String, provinceid:String, amphorid:String, districtid:String, postcode:String, latitude:String, longtitude:String, agencyid:String, address:String, website:String, email:String, space:String, feature:String, keyword:String, telnumber:String, date_open:String, description:String, counter_thai:String, counter_visitor:String, status:String, tourist_typename:String, province_name:String, amphur_name:String, district_name:String, agencyname:String, activityDatas:Vector.<ActivityData>, facilitiesDatas:Vector.<FacilitiesData>, utilityDatas:Vector.<UtilityData>, imagepath:String)
		{
			_touristid = touristid;
			_tourist_typeid = tourist_typeid;
			_tourist_subtypeid = tourist_subtypeid;
			_touristcode = touristcode;
			_tourist_pic = tourist_pic;
			_touristname = touristname;
			_clusterid = clusterid;
			_region_id = region_id;
			_provinceid = provinceid;
			_amphorid = amphorid;
			_districtid = districtid;
			_postcode = postcode;
			_latitude = latitude;
			_longtitude = longtitude;
			_agencyid = agencyid;
			_address = address;
			_website = website;
			_email = email;
			_space = space;
			_feature = feature;
			_keyword = keyword + " " + province_name;
			_telnumber = telnumber;
			_date_open = date_open;
			_description = description;
			_counter_thai = counter_thai;
			_counter_visitor = counter_visitor;
			_status = status;
			_tourist_typename = tourist_typename;
			_province_name = province_name;
			_amphur_name = amphur_name;
			_district_name = district_name;
			_agencyname = agencyname;
			_activity = activityDatas;
			_facilities = facilitiesDatas;
			_utility = utilityDatas;
			_imagepath = imagepath;
		}

		public function get utility():Vector.<UtilityData>
		{
			return _utility;
		}

		public function get facilities():Vector.<FacilitiesData>
		{
			return _facilities;
		}

		public function get activity():Vector.<ActivityData>
		{
			return _activity;
		}

		public function get imagepath():String
		{
			return _imagepath;
		}

		public function get touristid():String
		{
			return _touristid;
		}

		public function get tourist_typeid():String
		{
			return _tourist_typeid;
		}

		public function get tourist_subtypeid():String
		{
			return _tourist_subtypeid;
		}

		public function get touristcode():String
		{
			return _touristcode;
		}

		public function get tourist_pic():String
		{
			return _tourist_pic;
		}

		public function get touristname():String
		{
			return _touristname;
		}

		public function get clusterid():String
		{
			return _clusterid;
		}

		public function get region_id():String
		{
			return _region_id;
		}

		public function get provinceid():String
		{
			return _provinceid;
		}

		public function get amphorid():String
		{
			return _amphorid;
		}

		public function get districtid():String
		{
			return _districtid;
		}

		public function get postcode():String
		{
			return _postcode;
		}

		public function get latitude():String
		{
			return _latitude;
		}

		public function get longtitude():String
		{
			return _longtitude;
		}

		public function get agencyid():String
		{
			return _agencyid;
		}

		public function get address():String
		{
			return _address;
		}

		public function get website():String
		{
			return _website;
		}

		public function get email():String
		{
			return _email;
		}

		public function get space():String
		{
			return _space;
		}

		public function get feature():String
		{
			return _feature;
		}

		public function get keyword():String
		{
			return _keyword;
		}

		public function get telnumber():String
		{
			return _telnumber;
		}

		public function get date_open():String
		{
			return _date_open;
		}

		public function get description():String
		{
			return _description;
		}

		public function get counter_thai():String
		{
			return _counter_thai;
		}

		public function get counter_visitor():String
		{
			return _counter_visitor;
		}

		public function get status():String
		{
			return _status;
		}

		public function get tourist_typename():String
		{
			return _tourist_typename;
		}

		public function get province_name():String
		{
			return _province_name;
		}

		public function get amphur_name():String
		{
			return _amphur_name;
		}

		public function get district_name():String
		{
			return _district_name;
		}

		public function get agencyname():String
		{
			return _agencyname;
		}

		public function toObject():Object
		{
			return {touristid: _touristid, tourist_typeid: _tourist_typeid, tourist_subtypeid: _tourist_subtypeid, touristcode: _touristcode, tourist_pic: _tourist_pic, touristname: _touristname, clusterid: _clusterid, region_id: _region_id, provinceid: _provinceid, amphorid: _amphorid, districtid: _districtid, postcode: _postcode, latitude: _latitude, longtitude: _longtitude, agencyid: _agencyid, address: _address, website: _website, email: _email, space: _space, feature: _feature, keyword: _keyword, telnumber: _telnumber, date_open: _date_open, description: _description, counter_thai: _counter_thai, counter_visitor: _counter_visitor, status: _status, tourist_typename: _tourist_typename, province_name: _province_name, amphur_name: _amphur_name, district_name: _district_name, agencyname: _agencyname, activity: _activity, facilities: _facilities, utility: _utility, imagepath: _imagepath}
		}


	}
}
