package com.dotr.core.model.data
{

	public class UtilityData extends Object
	{
		private var _utilityid:String;
		private var _utilityname:String;
		private var _utility_type_id:String;
		private var _utility_type_name:String;

		public function UtilityData(utilityid:String, utilityname:String, utility_type_id:String, utility_type_name:String)
		{
			_utilityid = utilityid;
			_utilityname = utilityname;
			_utility_type_id = utility_type_id;
			_utility_type_name = utility_type_name;
		}

		public function get utility_type_name():String
		{
			return _utility_type_name;
		}

		public function get utility_type_id():String
		{
			return _utility_type_id;
		}

		public function get utilityname():String
		{
			return _utilityname;
		}

		public function get utilityid():String
		{
			return _utilityid;
		}

	}
}
