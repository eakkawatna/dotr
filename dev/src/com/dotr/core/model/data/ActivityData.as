package com.dotr.core.model.data
{

	public class ActivityData extends Object
	{
		private var _activityid:String;
		private var _activityname:String;
		private var _activity_type_id:String;
		private var _activity_type_name:String;

		public function ActivityData(activityid:String, activityname:String, activity_type_id:String, activity_type_name:String)
		{
			_activityid = activityid;
			_activityname = activityname;
			_activity_type_id = activity_type_id;
			_activity_type_name = activity_type_name;
		}

		public function get activity_type_name():String
		{
			return _activity_type_name;
		}

		public function get activity_type_id():String
		{
			return _activity_type_id;
		}

		public function get activityname():String
		{
			return _activityname;
		}

		public function get activityid():String
		{
			return _activityid;
		}


	}
}
