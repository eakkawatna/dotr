package com.dotr.core.model.data
{

	public class TouristTypeData extends Object
	{
		private var _tourist_typeid:String;
		private var _tourist_typename:String;

		public function TouristTypeData(tourist_typeid:String, tourist_typename:String)
		{
			_tourist_typeid = tourist_typeid;
			_tourist_typename = tourist_typename;
		}

		public function get tourist_typename():String
		{
			return _tourist_typename;
		}

		public function get tourist_typeid():String
		{
			return _tourist_typeid;
		}

	}
}
