package com.dotr.core.model.data
{

	public class AmphurData extends Object
	{
		private var _amphur_id:String;
		private var _amphur_name:String;
		private var _province_id:String;

		public function AmphurData(amphur_id:String, amphur_name:String, province_id:String)
		{
			_amphur_id = amphur_id;
			_amphur_name = amphur_name;
			_province_id = province_id;
		}

		public function get province_id():String
		{
			return _province_id;
		}

		public function get amphur_name():String
		{
			return _amphur_name;
		}

		public function get amphur_id():String
		{
			return _amphur_id;
		}

	}
}
