package com.dotr.core.model.data
{

	public class SubTouristTypeData extends Object
	{
		private var _sub_tourist_type_id:String;
		private var _sub_tourist_type_name:String;
		private var _tourist_typeid:String;

		public function SubTouristTypeData(sub_tourist_type_id:String, sub_tourist_type_name:String, tourist_typeid:String)
		{
			_sub_tourist_type_id = sub_tourist_type_id;
			_sub_tourist_type_name = sub_tourist_type_name;
			_tourist_typeid = tourist_typeid;
		}

		public function get tourist_typeid():String
		{
			return _tourist_typeid;
		}

		public function get sub_tourist_type_name():String
		{
			return _sub_tourist_type_name;
		}

		public function get sub_tourist_type_id():String
		{
			return _sub_tourist_type_id;
		}

	}
}
