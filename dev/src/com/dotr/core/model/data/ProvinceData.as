package com.dotr.core.model.data
{

	public class ProvinceData extends Object
	{
		private var _province_id:String;
		private var _province_name:String;
		private var _region_id:String;
		private var _pv_total:int = 0;

		public function ProvinceData(province_id:String, province_name:String, region_id:String, pv_total:int = 0)
		{
			_province_id = province_id;
			_province_name = province_name;
			_region_id = region_id;
			_pv_total = pv_total;
		}

		public function get pv_total():int
		{
			return _pv_total;
		}

		public function get region_id():String
		{
			return _region_id;
		}

		public function get province_name():String
		{
			return _province_name;
		}

		public function get province_id():String
		{
			return _province_id;
		}

	}
}
