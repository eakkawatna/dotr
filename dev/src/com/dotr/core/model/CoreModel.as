package com.dotr.core.model
{
	import com.dotr.core.model.data.AmphurData;
	import com.dotr.core.model.data.ClusterData;
	import com.dotr.core.model.data.ProvinceData;
	import com.dotr.core.model.data.RegionData;
	import com.dotr.core.model.data.SubDistrictData;
	import com.dotr.core.model.data.SubTouristTypeData;
	import com.dotr.core.model.data.TouristData;
	import com.dotr.core.model.data.TouristTypeData;
	import com.dotr.core.signals.ReceiveLoadLocalDataSignal;
	import com.dotr.core.signals.ReceiveSaveDataSignal;
	import com.dotr.core.signals.ReceiveSaveLocalDataSignal;
	import com.dotr.core.signals.RequestSaveDataSignal;
	import com.dotr.core.signals.RequestSaveImageSignal;
	import com.dotr.core.signals.RequestSaveLocalDataSignal;

	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;

	public class CoreModel extends Actor
	{
		[Inject]
		public var requestSaveLocalDataSignal:RequestSaveLocalDataSignal;

		[Inject]
		public var receiveSaveLocalDataSignal:ReceiveSaveLocalDataSignal;

		[Inject]
		public var requestSaveDataSignal:RequestSaveDataSignal;

		[Inject]
		public var receiveSaveDataSignal:ReceiveSaveDataSignal;

		[Inject]
		public var receiveLoadLocalDataSignal:ReceiveLoadLocalDataSignal;

		[Inject]
		public var requestSaveImageSignal:RequestSaveImageSignal;


		// external ------------------------------------------------------------------
		public var changeTotalSignal:Signal = new Signal();
		public var searchSignal:Signal = new Signal();
		public var touristDatas:Vector.<TouristData>;
		public var clusterDatas:Vector.<ClusterData>;
		public var provinceDatas:Vector.<ProvinceData>;
		public var amphurDatas:Vector.<AmphurData>;
		public var subDistrictDatas:Vector.<SubDistrictData>;
		public var regionDatas:Vector.<RegionData>;
		public var touristTypeDatas:Vector.<TouristTypeData>;
		public var subTouristTypeDatas:Vector.<SubTouristTypeData>;
		public var isOnline:Boolean;

		private var _tour_total:int;
		private var _searchDatas:Vector.<TouristData>;
		public var base_tour_total:int;

		public function get tour_total():int
		{
			return _tour_total;
		}

		public function set tour_total(value:int):void
		{
			_tour_total = value;
			changeTotalSignal.dispatch();
		}

		public function get searchDatas():Vector.<TouristData>
		{
			return _searchDatas;
		}

		public function set searchDatas(value:Vector.<TouristData>):void
		{
			_searchDatas = value;
			searchSignal.dispatch();
		}

	}
}
