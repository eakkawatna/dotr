package com.dotr.core.signals
{
	import org.osflash.signals.Signal;

	public class RequestSaveDataSignal extends Signal
	{
		public function RequestSaveDataSignal()
		{
			super(String, String);
		}
	}
}
