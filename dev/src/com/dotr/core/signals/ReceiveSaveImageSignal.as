package com.dotr.core.signals
{
	import org.osflash.signals.Signal;

	public class ReceiveSaveImageSignal extends Signal
	{
		public function ReceiveSaveImageSignal()
		{
			super();
		}
	}
}
