package com.dotr.core.signals
{
	import org.osflash.signals.Signal;

	public class ReceiveSaveLocalDataSignal extends Signal
	{
		public function ReceiveSaveLocalDataSignal()
		{
			super();
		}
	}
}
