package com.dotr.core.signals
{
	import org.osflash.signals.Signal;

	public class ReceiveLoadLocalDataSignal extends Signal
	{
		public function ReceiveLoadLocalDataSignal()
		{
			super();
		}
	}
}
