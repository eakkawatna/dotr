package com.dotr.core.signals
{
	import org.osflash.signals.Signal;

	public class RequestSaveLocalDataSignal extends Signal
	{
		public function RequestSaveLocalDataSignal()
		{
			super(String, String);
		}
	}
}
