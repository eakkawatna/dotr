package com.dotr.core.signals
{
	import flash.utils.ByteArray;

	import org.osflash.signals.Signal;

	public class RequestSaveImageSignal extends Signal
	{
		public function RequestSaveImageSignal()
		{
			super(ByteArray);
		}
	}
}
