package com.idealizm.manager
{
	import com.greensock.TweenLite;
	import com.sleepydesign.net.LoaderUtil;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.utils.Dictionary;

	import __AS3__.vec.Vector;

	import org.osflash.signals.Signal;

	public class ImageManager
	{
		public static var loadingSignal:Signal = new Signal(Boolean);
		private static var _isProgress:Boolean = false;
		private static var _loader:Loader;
		private static var _group_queue:Dictionary = new Dictionary();
		private static var _group_completes:Dictionary = new Dictionary();
		private static var _group_progress:Dictionary = new Dictionary();
		private static var _group:String;
		private static var _images:Dictionary = new Dictionary();
		private static var _isCash:Boolean = false;

		public static function loadImage(group:String, path:String, complete:Function):void
		{
			_group = group;

			if (!_group_queue[group])
			{
				_group_queue[group] = new Vector.<String>();
				_group_completes[group] = new Vector.<Function>();
				_group_progress[group] = false;
			}

			_group_queue[group].push(path);
			_group_completes[group].push(complete);

			if (_group_progress[_group])
				return;
			load();


		}

		public static function kill(group:String):void
		{
			_group_queue[group] = new Vector.<String>();
			_group_completes[group] = new Vector.<Function>();

			_group_progress[group] = false;

			loadingSignal.dispatch(false);
		}

		public static function load():void
		{
			if (_group_progress[_group])
			{
				loadingSignal.dispatch(false);
				return;
			}

			if (_group_queue[_group].length == 0)
			{
				loadingSignal.dispatch(false);
				return;
			}

			loadingSignal.dispatch(true);

			_group_progress[_group] = true;
			var path:String = _group_queue[_group][0];
			if (_images[path])
			{
				_group_completes[_group][0](_images[path]);
				reset();
			}
			else
				_loader = LoaderUtil.loadBinaryAsBitmap(path, onComplete);
		}

		private static function onComplete(event:Event):void
		{
			if (event.type == Event.COMPLETE)
			{
				if (_group_completes[_group].length == 0)
				{
					loadingSignal.dispatch(false);
					return;
				}

				var complete:Function = _group_completes[_group][0];

				var src:Bitmap = _loader.contentLoaderInfo.content as Bitmap;
				if (src)
				{
					complete(src);

					if (_isCash)
						_images[_group_queue[_group][0]] = src;

					TweenLite.to(src, .01, {onComplete: reset});
				}
				else
					reset();

			}
			if (event.type == IOErrorEvent.IO_ERROR)
			{
				//loadingSignal.dispatch(false);
				reset();
			}
		}

		private static function reset():void
		{
			_group_progress[_group] = false;
			if (_group_queue[_group].length > 0)
			{
				VectorUtil.removeItemAt(_group_queue[_group], 0);
				VectorUtil.removeItemAt(_group_completes[_group], 0);

				load();
			}
			else
				loadingSignal.dispatch(false);

		}

	}
}
