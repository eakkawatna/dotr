package com.idealizm.display.components
{

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	public class SDItem extends Sprite
	{
		protected var _data:Object;
		protected var _selectIndex:int;
		protected var _label:TextField;
		protected var _defaultColor:uint = 0xffffff;
		protected var _selectedColor:uint = 0xdddddd;
		protected var _rolloverColor:uint = 0xeeeeee;
		protected var _selected:Boolean;
		protected var _mouseOver:Boolean = false;

		private var _skin:Sprite;

		public function SDItem(data:Object = null)
		{
			_skin = new ComboBox_item_sp;
			addChild(_skin);
			_label = _skin.getChildByName("title_txt") as TextField;
			_data = data;
			draw();

			mouseChildren = false;
			mouseEnabled = true;
		}

		/**
		 * Initilizes the component.
		 */
		protected function init():void
		{
			//addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		}


		///////////////////////////////////
		// public methods
		///////////////////////////////////

		/**
		 * Draws the visual ui of the component.
		 */
		public function draw():void
		{
			/*super.draw();
			graphics.clear();

			if (_selected)
			{
			graphics.beginFill(_selectedColor);
			}
			else if (_mouseOver)
			{
			graphics.beginFill(_rolloverColor);
			}
			else
			{
			graphics.beginFill(_defaultColor);
			}
			graphics.drawRect(0, 0, width, height);
			graphics.endFill();
			*/
			if (_data == null)
				return;

			if (_data is String)
			{
				_label.text = _data as String;
			}
			else if (_data.hasOwnProperty("label") && _data.label is String)
			{
				_label.text = _data.label;
			}
			else
			{
				_label.text = _data.toString();
			}
		}




		///////////////////////////////////
		// event handlers
		///////////////////////////////////

		/**
		 * Called when the user rolls the mouse over the item. Changes the background color.
		 */
		protected function onMouseOver(event:MouseEvent):void
		{
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			_mouseOver = true;
		}

		/**
		 * Called when the user rolls the mouse off the item. Changes the background color.
		 */
		protected function onMouseOut(event:MouseEvent):void
		{
			removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			_mouseOver = false;
		}



		///////////////////////////////////
		// getter/setters
		///////////////////////////////////

		/**
		 * Sets/gets the string that appears in this item.
		 */
		public function set data(value:Object):void
		{
			_data = value;

			draw();
		}

		public function get data():Object
		{
			return _data;
		}

		/**
		 * Sets/gets whether or not this item is selected.
		 */
		public function set selected(value:Boolean):void
		{
			_selected = value;
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		/**
		 * Sets/gets the default background color of list items.
		 */
		public function set defaultColor(value:uint):void
		{
			_defaultColor = value;
		}

		public function get defaultColor():uint
		{
			return _defaultColor;
		}

		/**
		 * Sets/gets the selected background color of list items.
		 */
		public function set selectedColor(value:uint):void
		{
			_selectedColor = value;
		}

		public function get selectedColor():uint
		{
			return _selectedColor;
		}

		/**
		 * Sets/gets the rollover background color of list items.
		 */
		public function set rolloverColor(value:uint):void
		{
			_rolloverColor = value;
		}

		public function get rolloverColor():uint
		{
			return _rolloverColor;
		}

		public function get selectIndex():int
		{
			return _selectIndex;
		}

		public function set selectIndex(value:int):void
		{
			_selectIndex = value;
		}
	}
}
