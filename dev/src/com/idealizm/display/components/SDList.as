package com.idealizm.display.components
{
	import com.bit101.components.Style;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.SDSprite;

	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	import org.osflash.signals.Signal;

	public class SDList extends SDSprite
	{
		public var selectSignal:Signal = new Signal(int);
		protected var _items:Array;
		protected var _itemHolder:Sprite;
		protected var _selectedIndex:int = -1;
		protected var _defaultColor:uint = Style.LIST_DEFAULT;
		protected var _alternateColor:uint = Style.LIST_ALTERNATE;
		protected var _selectedColor:uint = Style.LIST_SELECTED;
		protected var _rolloverColor:uint = Style.LIST_ROLLOVER;
		protected var _alternateRows:Boolean = false;

		private var _skin:Sprite;
		private var _container:Sprite;

		private var _contentScroll:ScrollController;
		private var _width:Number;
		private var _height:Number;

		public function SDList(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number = 0, items:Array = null)
		{
			this.scaleX = this.scaleY = Config.ratio;
			_contentScroll = new ScrollController();

			_skin = new ComboBox_list_sp;
			addChild(_skin);

			_skin.height = 50 * 8;

			_container = new Sprite;
			addChild(_container);

			_itemHolder = new Sprite();
			_container.addChild(_itemHolder);

			if (items != null)
			{
				_items = items;
			}
			else
			{
				_items = new Array();
			}

			init();
		}

		/**
		 * Initilizes the component.
		 */
		public function move(xpos:Number, ypos:Number):void
		{
			x = Math.round(xpos);
			y = Math.round(ypos);
		}

		protected function init():void
		{
			setSize(_skin.width, _skin.height);
			makeListItems();
		}

		public function setSize(w:Number, h:Number):void
		{
			_width = w;
			_height = h;
		/*dispatchEvent(new Event(Event.RESIZE));
*/
		}

		private function setScroller():void
		{
			var containerRect:Rectangle = new Rectangle(0, 0, _width, _height);
			var contentRect:Rectangle = new Rectangle(0, 0, _itemHolder.width, _itemHolder.height);
			if (_itemHolder.height > containerRect.height)
			{
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.verticalScrollingEnabled = true;
				_contentScroll.addScrollControll(_itemHolder, _container, containerRect, contentRect, 1);
			}
			else
			{

				_skin.height = _itemHolder.height;
				setSize(_skin.width, _skin.height);
			}
		}


		/**
		 * Creates all the list items based on data.
		 */
		public function makeListItems():void
		{
			var item:SDItem;
			for (var i:int = 0; i < _items.length; i++)
			{

				item = new SDItem(_items[i]);
				item.x = 0;
				item.y = i * item.height;
				_itemHolder.addChild(item);
				item.selectIndex = i;
				item.defaultColor = _defaultColor;
				item.selectedColor = _selectedColor;
				item.rolloverColor = _rolloverColor;
				item.addEventListener(MouseEvent.CLICK, onSelect);
			}

			if (_items.length > 0)
			{
				setScroller();
			}
		}



		///////////////////////////////////
		// public methods
		///////////////////////////////////

		/**
		 * Draws the visual ui of the component.
		 */
		public function draw():void
		{

			_selectedIndex = Math.min(_selectedIndex, _items.length - 1);


			// panel
		/*	_panel.setSize(_width, _height);
			_panel.color = _defaultColor;
			_panel.draw();
*/
		}

		/**
		 * Adds an item to the list.
		 * @param item The item to add. Can be a string or an object containing a string property named label.
		 */
		public function addItem(item:Object):void
		{
			_items.push(item);
			//makeListItems();
		}

		///////////////////////////////////
		// event handlers
		///////////////////////////////////

		/**
		 * Called when a user selects an item in the list.
		 */
		protected function onSelect(event:Event):void
		{
			var lt:SDItem = event.target as SDItem;
			if (!lt)
				return;

			for (var i:int = 0; i < _itemHolder.numChildren; i++)
			{
				SDItem(_itemHolder.getChildAt(i)).selected = false;
			}

			lt.selected = true;
			_selectedIndex = lt.selectIndex;

			selectSignal.dispatch(_selectedIndex);
		}

		///////////////////////////////////
		// getter/setters
		///////////////////////////////////

		/**
		 * Sets / gets the index of the selected list item.
		 */
		public function set selectedIndex(value:int):void
		{
			if (value >= 0 && value < _items.length)
			{
				_selectedIndex = value;
			}
			else
			{
				_selectedIndex = -1;
			}
			dispatchEvent(new Event(Event.SELECT));
		}

		public function get selectedIndex():int
		{
			return _selectedIndex;
		}

		/**
		 * Sets / gets the item in the list, if it exists.
		 */
		public function set selectedItem(item:Object):void
		{
			var index:int = _items.indexOf(item);
			//			if(index != -1)
			//			{
			selectedIndex = index;
			//			}
		}

		public function get selectedItem():Object
		{
			if (_selectedIndex >= 0 && _selectedIndex < _items.length)
			{
				return _items[_selectedIndex];
			}
			return null;
		}

		/**
		 * Sets/gets the default background color of list items.
		 */
		public function set defaultColor(value:uint):void
		{
			_defaultColor = value;
		}

		public function get defaultColor():uint
		{
			return _defaultColor;
		}

		/**
		 * Sets/gets the selected background color of list items.
		 */
		public function set selectedColor(value:uint):void
		{
			_selectedColor = value;
		}

		public function get selectedColor():uint
		{
			return _selectedColor;
		}

		/**
		 * Sets/gets the rollover background color of list items.
		 */
		public function set rolloverColor(value:uint):void
		{
			_rolloverColor = value;
		}

		public function get rolloverColor():uint
		{
			return _rolloverColor;
		}


		/**
		 * Sets / gets the list of items to be shown.
		 */
		public function set items(value:Array):void
		{
			_items = value;
		}

		public function get items():Array
		{
			return _items;
		}

		/**
		 * Sets / gets the color for alternate rows if alternateRows is set to true.
		 */
		public function set alternateColor(value:uint):void
		{
			_alternateColor = value;
		}

		public function get alternateColor():uint
		{
			return _alternateColor;
		}

		/**
		 * Sets / gets whether or not every other row will be colored with the alternate color.
		 */
		public function set alternateRows(value:Boolean):void
		{
			_alternateRows = value;
		}

		public function get alternateRows():Boolean
		{
			return _alternateRows;
		}

	}


}

