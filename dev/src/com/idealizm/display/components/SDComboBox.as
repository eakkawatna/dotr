package com.idealizm.display.components
{
	import com.bit101.components.Component;
	import com.sleepydesign.display.DisplayObjectUtil;

	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import org.osflash.signals.Signal;

	public class SDComboBox extends Component
	{
		public static const TOP:String = "top";
		public static const BOTTOM:String = "bottom";

		public var changeSignal:Signal = new Signal;
		protected var _defaultLabel:String = "";
		protected var _dropDownButton:Sprite;
		protected var _items:Array;
		protected var _labelButton:TextField;
		protected var _list:SDList;
		protected var _numVisibleItems:int = 10;
		protected var _open:Boolean = false;
		protected var _openPosition:String = BOTTOM;
		protected var _stage:Stage;
		private var _skin:Sprite;

		public function SDComboBox(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number = 0, defaultLabel:String = "", items:Array = null)
		{
			_skin = new ComboBox_skin_sp;
			addChild(_skin);

			_width = _skin.width;
			_height = _skin.height;

			_defaultLabel = defaultLabel;
			_items = items;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			super(parent, xpos, ypos);
		}

		/**
		 * Initilizes the component.
		 */
		protected override function init():void
		{
			super.init();
			setSize(_skin.width, _skin.height);
			setLabelButtonLabel();
		}

		/**
		 * Creates and adds the child display objects of this component.
		 */
		protected override function addChildren():void
		{
			super.addChildren();
			_list = new SDList(null, 0, 0, _items);
			_list.selectSignal.add(onSelect);
			//_list.autoHideScrollBar = true;
			//_list.addEventListener(Event.SELECT, onSelect);

			_labelButton = _skin.getChildByName("title_txt") as TextField;
			_dropDownButton = _skin.getChildByName("list_btn") as Sprite; //new PushButton(this, 0, 0, "+", onDropDown);

			_dropDownButton.addEventListener(MouseEvent.CLICK, onDropDown);
			_labelButton.text = _defaultLabel;
		}

		/**
		 * Determines what to use for the main button label and sets it.
		 */
		protected function setLabelButtonLabel():void
		{
			if (selectedItem == null)
			{
				_labelButton.text = _defaultLabel;
			}
			else if (selectedItem is String)
			{
				_labelButton.text = selectedItem as String;
			}
			else if (selectedItem.hasOwnProperty("label") && selectedItem.label is String)
			{
				_labelButton.text = selectedItem.label;
			}
			else
			{
				_labelButton.text = selectedItem.toString();
			}

			changeSignal.dispatch();
		}

		/**
		 * Removes the list from the stage.
		 */
		protected function removeList():void
		{
			if (_stage.contains(_list))
				_stage.removeChild(_list);
			_stage.removeEventListener(MouseEvent.CLICK, onStageClick);
			//_dropDownButton.label = "+";
		}



		///////////////////////////////////
		// public methods
		///////////////////////////////////

		public override function draw():void
		{
			super.draw();
			//_list.setSize(_width, _numVisibleItems * _list.listItemHeight);
		}


		/**
		 * Adds an item to the list.
		 * @param item The item to add. Can be a string or an object containing a string property named label.
		 */
		public function addItem(item:Object):void
		{
			trace("item" + item);
			_list.addItem(item);
		}


		///////////////////////////////////
		// event handlers
		///////////////////////////////////

		/**
		 * Called when one of the top buttons is pressed. Either opens or closes the list.
		 */
		protected function onDropDown(event:MouseEvent):void
		{
			_open = !_open;
			if (_open)
			{
				var point:Point = new Point();
				/*	if (_openPosition == BOTTOM)
					{*/
				point.y = _height;
				/*	}
					else
					{
						point.y = -_numVisibleItems * _list.listItemHeight;
					}*/
				point = this.localToGlobal(point);
				_list.move(point.x, point.y);
				_stage.addChild(_list);

				//_dropDownButton.label = "-";

				_stage.addEventListener(MouseEvent.CLICK, onStageClick);
			}
			else
			{
				removeList();
			}
		}

		/**
		 * Called when the mouse is clicked somewhere outside of the combo box when the list is open. Closes the list.
		 */
		protected function onStageClick(event:MouseEvent):void
		{
			// ignore clicks within buttons or list
			if (event.target == _dropDownButton || event.target == _labelButton)
				return;
			if (new Rectangle(_list.x, _list.y, _list.width, _list.height).contains(event.stageX, event.stageY))
				return;

			_open = false;
			removeList();
		}

		/**
		 * Called when an item in the list is selected. Displays that item in the label button.
		 */
		protected function onSelect(value:int):void
		{
			_open = false;
			//_dropDownButton.label = "+";
			if (stage != null && stage.contains(_list))
			{
				stage.removeChild(_list);
			}
			setLabelButtonLabel();
		}

		/**
		 * Called when the component is added to the stage.
		 */
		protected function onAddedToStage(event:Event):void
		{
			_stage = stage;
		}

		/**
		 * Called when the component is removed from the stage.
		 */
		protected function onRemovedFromStage(event:Event):void
		{
			removeList();
		}

		///////////////////////////////////
		// getter/setters
		///////////////////////////////////

		/**
		 * Sets / gets the index of the selected list item.
		 */
		public function set selectedIndex(value:int):void
		{
			_list.selectedIndex = value;
			setLabelButtonLabel();
		}

		public function get selectedIndex():int
		{
			return _list.selectedIndex;
		}

		/**
		 * Sets / gets the item in the list, if it exists.
		 */
		public function set selectedItem(item:Object):void
		{
			_list.selectedItem = item;
			setLabelButtonLabel();
		}

		public function get selectedItem():Object
		{

			return _list.selectedItem;
		}

		/**
		 * Sets/gets the default background color of list items.
		 */
		public function set defaultColor(value:uint):void
		{
			_list.defaultColor = value;
		}

		public function get defaultColor():uint
		{
			return _list.defaultColor;
		}

		/**
		 * Sets/gets the selected background color of list items.
		 */
		public function set selectedColor(value:uint):void
		{
			_list.selectedColor = value;
		}

		public function get selectedColor():uint
		{
			return _list.selectedColor;
		}

		/**
		 * Sets/gets the rollover background color of list items.
		 */
		public function set rolloverColor(value:uint):void
		{
			_list.rolloverColor = value;
		}

		public function get rolloverColor():uint
		{
			return _list.rolloverColor;
		}


		/**
		 * Sets / gets the position the list will open on: top or bottom.
		 */
		public function set openPosition(value:String):void
		{
			_openPosition = value;
		}

		public function get openPosition():String
		{
			return _openPosition;
		}

		/**
		 * Sets / gets the label that will be shown if no item is selected.
		 */
		public function set defaultLabel(value:String):void
		{
			_defaultLabel = value;
			setLabelButtonLabel();
		}

		public function get defaultLabel():String
		{
			return _defaultLabel;
		}

		/**
		 * Sets / gets the number of visible items in the drop down list. i.e. the height of the list.
		 */
		public function set numVisibleItems(value:int):void
		{
			_numVisibleItems = Math.max(1, value);
			invalidate();
		}

		public function get numVisibleItems():int
		{
			return _numVisibleItems;
		}

		/**
		 * Sets / gets the list of items to be shown.
		 */
		public function set items(value:Array):void
		{
			_list.items = value;
		}

		public function get items():Array
		{
			return _list.items;
		}

		/**
		 * Sets / gets the color for alternate rows if alternateRows is set to true.
		 */
		public function set alternateColor(value:uint):void
		{
			_list.alternateColor = value;
		}

		public function get alternateColor():uint
		{
			return _list.alternateColor;
		}

		/**
		 * Sets / gets whether or not every other row will be colored with the alternate color.
		 */
		public function set alternateRows(value:Boolean):void
		{
			_list.alternateRows = value;
		}

		public function get alternateRows():Boolean
		{
			return _list.alternateRows;
		}


		/**
		 * Gets whether or not the combo box is currently open.
		 */
		public function get isOpen():Boolean
		{
			return _open;
		}

		public function dispose():void
		{
			_items = null;
			changeSignal.removeAll();
			_stage.removeEventListener(MouseEvent.CLICK, onStageClick);
			_dropDownButton.removeEventListener(MouseEvent.CLICK, onDropDown);

			DisplayObjectUtil.removeChildren(this, true, true);

			try
			{
				if (parent != null)
					parent.removeChild(this);
			}
			catch (e:*)
			{
			}
		}
	}


}
