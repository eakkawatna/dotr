package com.idealizm.display.effect
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.utils.Dictionary;

	public class Effect
	{
		private static var _display:DisplayObject;
		private static var _current_value:int;
		private static var _current_value_dic:Dictionary = new Dictionary();
		private static var _displayList:Dictionary = new Dictionary();
		private static var _sizeList:Dictionary = new Dictionary();

		public static function addGlow(display:DisplayObject, size1:int = 40, size2:int = 20):void
		{
			TweenPlugin.activate([GlowFilterPlugin]);
			_displayList[display.name] = display;
			_sizeList[display.name] = new Array(size1, size2);
			glowLoop(display);
		}

		public static function glowLoop(display:DisplayObject):void
		{
			var size:Array = _sizeList[display.name];
			var value:int = (_current_value_dic[display.name] != size[0]) ? size[0] : size[1];

			TweenMax.to(display, 1, {glowFilter: {color: 0xffffff, alpha: 1, blurX: value, blurY: value + value / 2}, onComplete: glowLoop, onCompleteParams: [display]});
			_current_value_dic[display.name] = value;
		}

		public static function addGlowAndBounceeLoop(display:DisplayObject, value:int = 15):void
		{
			TweenPlugin.activate([GlowFilterPlugin]);
			_displayList[display.name] = display;
			var scale:Number = (display.scaleX != 1) ? 1 : 1.2;

			TweenMax.killTweensOf(display);
			TweenMax.to(display, .5, {scaleX: scale, scaleY: scale, glowFilter: {color: 0xffffff, alpha: 1, blurX: value, blurY: value}, onComplete: addGlowAndBounceeLoop, onCompleteParams: [display]});
		}

		public static function addBounceeLoop(display:DisplayObject, value:int = 15):void
		{
			_displayList[display.name] = display;
			var scale:Number = (display.scaleX != 1) ? 1 : 1.2;

			TweenLite.killTweensOf(display);
			TweenLite.to(display, .5, {scaleX: scale, scaleY: scale, onComplete: addBounceeLoop, onCompleteParams: [display]});
		}

		public static function removeGlow():void
		{
			if (!_display)
				return;

			TweenMax.to(_display, 0, {glowFilter: {remove: true}, onComplete: function():void
			{
				TweenMax.killTweensOf(_display);
			}});
		}

		public static function removeGlowByItem(display:DisplayObject, isScale:Boolean = true):void
		{
			TweenMax.to(display, 0, {alpha: 1, glowFilter: {remove: true}, onComplete: function():void
			{
				if (isScale)
				{
					display.scaleX = 1;
					display.scaleY = 1;
				}
				TweenMax.killTweensOf(display);
			}});
		}

		public static function removeGlowAll():void
		{
			for each (var item:* in _displayList)
				TweenMax.killTweensOf(item);
		}

		public static function addGlowMnini(display:Sprite):void
		{
			TweenPlugin.activate([GlowFilterPlugin]);
			glowLoopMini(display);
		}

		public static function glowLoopMini(display:Sprite):void
		{
			var glow:GlowFilter = new GlowFilter();
			glow.color = 0xffffff;
			glow.alpha = 1;
			glow.blurX = 5;
			glow.blurY = 5;
			glow.quality = BitmapFilterQuality.MEDIUM;

			display.filters = [glow];
		}


	}
}


