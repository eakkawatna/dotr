package com.sleepydesign.lightboxs.signals
{
	import flash.display.DisplayObject;

	import org.osflash.signals.Signal;

	public class LightBoxSignal extends Signal
	{
		public function LightBoxSignal()
		{
			super(DisplayObject, String);
		}
	}
}
